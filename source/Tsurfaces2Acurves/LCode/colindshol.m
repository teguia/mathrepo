%This function returns the indices of the variables which are in the
%holomorphicity equation corresponding to the square whose bottom left
%corner is at index (i, j).
function inds = colindshol(indices, i, j)
    formatSpec = "%d, %d";
    key1 = sprintf(formatSpec, i+1, j+1);
    key2 = sprintf(formatSpec, i, j);
    key3 = sprintf(formatSpec, i, j+1);
    key4 = sprintf(formatSpec, i+1, j);
    inds = [indices(key1), indices(key2), indices(key3), indices(key4)];
end