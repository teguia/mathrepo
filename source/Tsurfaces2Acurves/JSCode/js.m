tic %start time
g = 5; %Genus of JS representative given by unit squares
n = 0; %n = level of approximation


%make sparse matrix of coefficients
M = matrix(g, n);

%make matrix of b vectors: ith column = ith vector
nonormstotal = (4*g-4)*3^n*3^n + (g-1)*4*(3^n) + 4*g-4 + 3^n + 1 + 2*g+2;
brow = nonormstotal+1:nonormstotal+g;
bcol = 1:g;
bdat = ones(1, g);
B = sparse(brow, bcol, bdat, nonormstotal+g, g);

%numvars = the total number  of variables
numvars = (4*g-4)*3^n*3^n + (4*g-4)*3^n + 3^n + 1 + 5*g;

%make solution matrix by 
sols = zeros(g);
for i = 1:g
    b = B(:, i);
    x = M\b;
    %if i>g k = i-g; else k=i; end
    for j = 1:g
        sols(i, j) = sols(i, j) + x(numvars-g+j);
        %i, j, sols %Un comment for outputting each i,j, and solutions
    end
end


n %print n
sols %print the nth level discrete Riemann matrix

toc %end time


writematrix(sols,'g5n7.txt','Delimiter',';')   %write the solutions matrix to a .txt file