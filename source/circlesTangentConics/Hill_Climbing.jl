using JuMP
using GLPK
using HomotopyContinuation
using LinearAlgebra
using Convex
using SCS

@var s, t, r # Circle centered at (s, t) with radius r
@var u[1:3], v[1:3] # Three points of tangency (u[1], v[1]), ..., (u[3], v[3])
@var a[1:6], b[1:6], c[1:6] # Three fixed conics with coefficients defined by a[1], ..., a[6]; ... ; c[1], ..., c[6]

#=
Set up the polynomial system defining tritangent circles
=#

f_1 = (u[1] - s)^2 + (v[1] - t)^2 - r # the point (u[1], v[1]) lies on the circle

f_2 = (u[2] - s)^2 + (v[2] - t)^2 - r # the point (u[2], v[2]) lies on the circle

f_3 = (u[3] - s)^2 + (v[3] - t)^2 - r # the point (u[3], v[3]) lies on the circle

f_4 = a[1]*u[1]^2 + a[2]*u[1]*v[1] + a[3]*v[1]^2 + a[4]*u[1] + a[5]*v[1] + a[6] # the point (u[1], v[1]) lies on the conic defined by coefficients a[1:6]

f_5 = b[1]*u[2]^2 + b[2]*u[2]*v[2] + b[3]*v[2]^2 + b[4]*u[2] + b[5]*v[2] + b[6] # the point (u[2], v[2]) lies on the conic defined by coefficients b[1:6]

f_6 = c[1]*u[3]^2 + c[2]*u[3]*v[3] + c[3]*v[3]^2 + c[4]*u[3] + c[5]*v[3] + c[6] # the point (u[3], v[3]) lies on the conic defined by coefficients c[1:6]

f_7 = det([differentiate(f_1, [u[1], v[1]]) differentiate(f_4, [u[1], v[1]])]) # Circle and conic are tangent at (u[1], v[1])

f_8 = det([differentiate(f_2, [u[2], v[2]]) differentiate(f_5, [u[2], v[2]])]) # Circle and conic are tangent at (u[2], v[2])

f_9 = det([differentiate(f_3, [u[3], v[3]]) differentiate(f_6, [u[3], v[3]])]) # Circle and conic are tangent at (u[3], v[3])

# Polynomial system in variables  [u[1], v[1], u[2], v[2], u[3], v[3], s, t, r] and parameters [a[1], ..., a[6], b[1], ..., b[6], c[1], ..., c[6]]

F = System([f_1, f_2, f_3, f_4, f_5, f_6, f_7, f_8, f_9], variables = [u[1], v[1], u[2], v[2], u[3], v[3], s, t, r], 
parameters = [a[1], a[2], a[3], a[4], a[5], a[6], b[1], b[2], b[3], b[4], b[5], b[6], c[1], c[2], c[3], c[4], c[5], c[6]]);

# Polynomial system in variables [a[1], ..., a[6], b[1], ..., b[6], c[1], ..., c[6]] and parameters [u[1], v[1], u[2], v[2], u[3], v[3], s, t, r]

G = System([f_1, f_2, f_3, f_4, f_5, f_6, f_7, f_8, f_9], variables = [a[1], a[2], a[3], a[4], a[5], a[6], b[1], b[2], b[3], b[4], b[5], b[6], c[1], c[2], c[3], c[4], c[5], c[6]], 
parameters = [u[1], v[1], u[2], v[2], u[3], v[3], s, t, r])

# Jacobian of polynomial system with respect to variables

function jacobian_vars(a, u)
    return jacobian(F, u, a)
end

# Jacobian of polynomial system with respect to parameters

function jacobian_params(a, u)
    return jacobian(G, a, u)
end

# Block jacobian for F, returns 18x18 matrix with upper-left block real parts and lower-left block imaginary parts

function block_jacobian_vars(a, u)
    J = jacobian(F, u, a)
    M = zeros(18, 18)
    M[1:9, 1:9] = real(J)
    M[10:18, 10:18] = imag(J)
    return M
end

# Block jacobian for G, returns 18x18 matrix with first 9 rows real parts and last 9 rows imaginary parts

function block_jacobian_params(a, u)
    J = jacobian(G, a, u)
    M = zeros(18, 18)
    M[1:9, 1:18] = real(J)
    M[10:18, 1:18] = imag(J)
    return M
end


#=
Algorithm 1

Inputs: complex root, parameters for conics, real solutions (as an array), complex solutions (as an array)
Outputs: complex root, parameters for conics, real solutions (as an array), complex solutions (as an array)

Algorithm 1 takes in a distinguished complex root, parameters for conics, real solutions, and complex solutions. 
The algorithm finds new parameters for conics that minimize the complex part of the distinguished root within a 
neighborhood of a user-specified p_max of the original set of parameters. The algorithm then tracks the each of 
the solutions given the new parameters using the path tracking option of HomotopyContinuation.jl. 
=#
function algorithm1(root, p, real_sols, complex_sols)
    try
        p_max = 10^-3

        lr = length(r_sols)
        lc = length(c_sols)

        model = Model(GLPK.Optimizer)

        @variable(model, -p_max <= d_p[1:18] <= p_max)
        @variable(model, d_img[1:9])
        @variable(model, d_real[1:lr, 1:9])
        @variable(model, s[1:9] >= 0)
        @variable(model, t[1:9] >= 0)
        @variable(model, d_x[1:lc, 1:18])

        @objective(model, Min, dot(s+t, ones(9)))

        @constraint(model, s - t .== d_img)

        @constraint(model, imag(jacobian_vars(p, root))*d_img + imag(jacobian_params(p, root))*d_p .== 0)

        @constraint(model, c[i=1:lr, j=i:lr], dot(real_sols[i]-real_sols[j], d_real[i:i,1:end]-d_real[j:j, 1:end]) >= 0)

        @constraint(model, d[i=1:lr], (jacobian_vars(p, real_sols[i])*d_real[i:i, 1:end]')[1:end] + jacobian_params(p, real_sols[i])*d_p .== 0)

        @constraint(model, e[i=1,lc], (block_jacobian_vars(p, root) * (d_x[i:i, 1:end])')[1:end] + block_jacobian_params(p, root)*d_p .== 0)
        
        @constraint(model, f[i=1,lc], collect(Iterators.flatten([real(complex_sols[i]), imag(complex_sols[i])])) * d_x[i:i, 1:end] .>= 0)

        optimize!(model)

        n_p = value.(d_p)+p
        n_root = solutions(solve(F, root, start_parameters = p, target_parameters = n_p, tracker_options = TrackerOptions(automatic_differentiation = 8, max_steps = 1000000, parameters = :conservative, extended_precision = true), show_progress = false))
        n_r_sols = real_solutions(solve(F, r_sols, start_parameters = p, target_parameters = n_p, tracker_options = TrackerOptions(automatic_differentiation = 8, max_steps = 1000000, parameters = :conservative, extended_precision = true), show_progress = false))
        n_c_sols = solutions(solve(F, c_sols, start_parameters = p, target_parameters = n_p, tracker_options = TrackerOptions(automatic_differentiation = 8, max_steps = 1000000, parameters = :conservative, extended_precision = true), show_progress = false))

        return n_root[1], n_p, n_r_sols, n_c_sols
    catch
        print("(Algorithm 1) The linear program finds no solutions.")
    end
end

#=
Inputs: two real solutions to be separated, parameters for conics, real solutions (as an array), complex solutions (as an array)
Outputs: parametesr for conics

Algorithm 2 takes in a pair of real solutions, parameters for conics, real solutions, and complex solutions. 
The algorithm finds new parameters for conics that maximize the distance between the two distinguished real roots within a 
neighborhood of a user-specified p_max of the original set of parameters.  
=#

function algorithm2(root1, root2, p, real_sols, complex_sols)
    try
    
        lr = length(real_sols)
        lc = length(complex_sols)

        p_max = 10^-3

        model = Model(GLPK.Optimizer)

        @variable(model, -p_max <= d_p[1:18] <= p_max)
        @variable(model, d_real[1:lr, 1:9])
        @variable(model, d_x[1:lc, 1:18])

        eqn = dot(root1 - root2, d_real[end-1:end-1, 1:end] - d_real[end:end, 1:end])

        @objective(model, Max, eqn)

        @constraint(model, c[i=1:lr, j=i:lr], dot(real_sols[i]-real_sols[j], d_real[i:i,1:end]-d_real[j:j, 1:end]) >= 0)

        @constraint(model, d[i=1:lr], (jacobian_vars(p, real_sols[i])*d_real[i:i, 1:end]')[1:end] + jacobian_params(p, real_sols[i])*d_p .== 0) #constraint on the real roots already there

        @constraint(model, (jacobian_vars(p, root1)*d_real[end-1:end-1, 1:end]')[1:end] + jacobian_params(p, root1)*d_p .== 0) # constraint on the new real root "root1"
        
        @constraint(model, (jacobian_vars(p, root2)*d_real[end:end, 1:end]')[1:end] + jacobian_params(p, root2)*d_p .== 0) # constraint on the new real root "root2"

        @constraint(model, e[i=1,lc], (block_jacobian_vars(p, complex_sols[i]) * (d_x[i:i, 1:end])')[1:end] + block_jacobian_params(p, complex_sols[i])*d_p .== 0)
        
        @constraint(model, f[i=1,lc], collect(Iterators.flatten([real(complex_sols[i]), imag(complex_sols[i])])) * d_x[i:i, 1:end] .>= 0)

        optimize!(model)


        return value.(d_p)+p
    catch
        print("(Algorithm 2) The linear program finds no solutions.")
    end
end

#=
Inputs: parameters defining conics
Outputs: parameters defining conics

Algoloop takes in a set of parameters defining conics, repeatedly finds parameters that minimize a distinguished complex root until the imaginary part is sufficiently small,

=#

function algoloop(p)

    S = solve(F, target_parameters = p, show_progress = false);

    rindex = findall(s -> is_real(s) && is_nonsingular(s), S)
    cindex = findall(s -> !is_real(s) && is_nonsingular(s), S)

    r_sols = solutions(S[rindex])
    r_sols = real.(r_sols)
    c_sols = solutions(S[cindex])

    root = c_sols[1]

    while norm(imag(root), Inf) > 10^-30
        try
            global root, p, r_sols, c_sols = algorithm1(root, p, r_sols, c_sols)
        catch
            r1 = real(root) .+ 10^-5
            r2 = real(root) .- 10^-5

            q = Convex.Variable(18)

            problem = minimize(sumsquares(p-q))

            problem.constraints += q[1]*r1[1]^2 + q[2]*r1[1]*r1[2] + q[3]*r1[2]^2 + q[4]*r1[1] + q[5]*r1[2] + q[6] == 0
            problem.constraints += q[1]*r2[1]^2 + q[2]*r2[1]*r2[2] + q[3]*r2[2]^2 + q[4]*r2[1] + q[5]*r2[2] + q[6] == 0
            problem.constraints += q[7]*r1[3]^2 + q[8]*r1[3]*r1[4] + q[9]*r1[4]^2 + q[10]*r1[3] + q[11]*r1[4] + q[12] == 0
            problem.constraints += q[7]*r2[3]^2 + q[8]*r2[3]*r2[4] + q[9]*r2[4]^2 + q[10]*r2[3] + q[11]*r2[4] + q[12] == 0
            problem.constraints += q[13]*r1[5]^2 + q[14]*r1[5]*r1[6] + q[15]*r1[6]^2 + q[16]*r1[5] + q[17]*r1[6] + q[18] == 0
            problem.constraints += q[13]*r2[5]^2 + q[14]*r2[5]*r2[6] + q[15]*r2[6]^2 + q[16]*r2[5] + q[17]*r2[6] + q[18] == 0
            problem.constraints += (q[5] + r1[1]*q[2] + 2*r1[2]*q[3])*(-r1[7] + r1[1]) - (q[4] + 2*r1[1]*q[1] + r1[2]*q[2])*(-r1[8] + r1[2]) == 0
            problem.constraints += (q[5] + r2[1]*q[2] + 2*r2[2]*q[3])*(-r2[7] + r2[1]) - (q[4] + 2*r2[1]*q[1] + r2[2]*q[2])*(-r2[8] + r2[2]) == 0
            problem.constraints += (q[11] + r1[3]*q[8] + 2*r1[4]*q[9])*(-r1[7] + r1[3]) - (q[10] + 2*r1[3]*q[7] + r1[4]*q[8])*(-r1[8] + r1[4]) == 0
            problem.constraints += (q[11] + r2[3]*q[8] + 2*r2[4]*q[9])*(-r2[7] + r2[3]) - (q[10] + 2*r2[3]*q[7] + r2[4]*q[8])*(-r2[8] + r2[4]) == 0
            problem.constraints += (q[17] + r1[5]*q[14] + 2*r1[6]*q[15])*(-r1[7] + r1[5]) - (q[16] + 2*r1[5]*q[13] + r1[6]*q[14])*(-r1[8] + r1[6]) == 0
            problem.constraints += (q[17] + r2[5]*q[14] + 2*r2[6]*q[15])*(-r2[7] + r2[5]) - (q[16] + 2*r2[5]*q[13] + r2[6]*q[14])*(-r2[8] + r2[6]) == 0


            Convex.solve!(problem, SCS.Optimizer)

            p_ = round.(Convex.evaluate(q), digits = 14)

            S_ = solve(F, target_parameters = p_);

            rindex_ = findall(t -> is_real(t) && is_nonsingular(t), S_)
            cindex_ = findall(t -> !is_real(t) && is_nonsingular(t), S_)

            r_sols_ = solutions(S[rindex_])
            r_sols_ = real.(r_sols_)
            c_sols_ = solutions(S[cindex_])
        
            return algorithm2(r1, r2, p_, r_sols_, c_sols_)
        end
    end
end
