==================================
Real Circles Tangent to 3 Conics
==================================

| This page contains the code described in the paper: Real circles tangent to 3 conics
| Authors: Paul Breiding, Julia Lindberg, Wern Juin Gabriel Ong, Linus Sommer
| ARXIV: TBD
| CODE: https://mathrepo.mis.mpg.de/circlesTangentConics

.. |code1| replace:: :download:`Certify_136_Real_Solutions.jl <Certify_136_Real_Solutions.jl>`

The file |code1| certifies an instance of three conics that have 136 real tritangent circles with parameters described in Theorem 1.4.

.. |code2| replace:: :download:`Hill_Climbing.jl <Hill_Climbing.jl>`

The file |code2| runs the hill climbing algorithm described in Algorithm 2.

.. |code3| replace:: :download:`certify.csv <certify.csv>`
.. |code9| replace:: :download:`certify.jl <certify.jl>`

The file |code3| gives the certification data needed for the proof of Theorem 3.1 and
the file |code9| computes the certification for each instance.

The page `Hill_Climb_Data_Generation <Hill_Climb_Data_Generation.html>`_ generates the hill climbing data needed in Section 4 outlined in Algorithm 3.
It is a notebook that generates hill climbing data as a CSV file where the 1st 18 columns are the
input to the hill climbing algorithm and the last 18 columns are the output of the hill climbing algorithm after one step.


The page `Real_Solution_Counts <Real_Solution_Counts.html>`_ is a notebook that takes the CSV file output by Hill_Climb_Data_Generation and produces
a CSV file where the 1st 18 columns are the input to the hill-climbing algorithm, the next 18 columns are
the output of the hill climbing algorithm after one step, and the final column is the number of real solutions.

.. |code6| replace:: :download:`data_hill_climbing.csv <data_hill_climbing.csv>`

The file |code6| gives the data set :math:`\mathcal{D}_1` used in Section 4.

.. |code7| replace:: :download:`data_random.csv <data_random.csv>`

The file |code7| gives the data set :math:`\mathcal{D}_2` used in Section 4.


The page `neural_count <neural_count.html>`_ trains the machine learning model outlined in Section 4
using the data sets :math:`\mathcal{D}_1, \mathcal{D}_2` and :math:`\mathcal{D}_1 \cup \mathcal{D}_2`.


The page `analysis <analysis.html>`_ is used to find the prediction errors on the validation data.

Project page created: November 10, 2022

Project contributors: Paul Breiding, Julia Lindberg, Wern Juin Gabriel Ong, Linus Sommer

Software used: Julia (v.1.7)

Corresponding author of this page: Julia Lindberg, julia.lindberg@mis.mpg.de
