####################
 Singular
####################

-  :doc:`ligands/index`

-  :doc:`tropicalProjections/index`

-  :doc:`OSCAR/index`

-  :doc:`ToricDegenerationsCubics/index`

-  :doc:`tropicalBases/index`

-  :doc:`tropicalModifications/index`

-  :doc:`EulerIntegrals/index`

.. toctree::
   :maxdepth: 0
