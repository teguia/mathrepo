==============================================================
Some examples
==============================================================

This page contains some illustrative examples for counting number of lines of random cubic surface over the field of 7-adic numbers and the field of real numbers. 

.. code-block:: none

						We compute the number of lines of a random cubic surface distributed using the Haar measure where the coefficients are random i.i.d variables over `Z_7` as follows:
						               	
								----------------------------------------------------
								----------------------------------------------------

								-- Example 1: Haar measure
						                 -- input:
								f:= randomSmoothCubicSurface(7,7,powers());
						                 NumberOfLines := howManyLines(f,7);
						                
						                 --output:

						                 f = 252281*x^3 + 214713*x^2*y + 3422676*x*y^2 + 4659584*y^3 + 2015147*x^2*z + 4412391*x*y*z + 4599736*y^2*z + 4147582*x*z^2 + 
						                 5130557*y*z^2 + 5704579*z^3 + 2377154*x^2*w + 5119094*x*y*w + 188858*y^2*w + 3600096*x*z*w + 2612439*y*z*w + 3434288*z^2*w + 
						                 1901101*x*w^2 + 1047958*y*w^2 + 5613822*z*w^2 + 5742524*w^3

						                 NumberOfLines = 1            
								----------------------------------------------------
								----------------------------------------------------

						We compute the number of lines of a random cubic surface distributed using the Blow up measure where first we construct a random polynomial of degree 6 and then blow up the projective plane on the roots of that polynomial:		
								
								----------------------------------------------------
								----------------------------------------------------
								-- Example 2: Blow up Measure
						                 -- input:
						                 g:= randomPolynomial(7,7);
						                 NumberOfLines:= howManyLines(g);

						                 --output: 
						                 g = (2111020 + O(7^300))*x^6 + (4823503 + O(7^300))*x^5 + (122887 + O(7^300))*x^4 + (5575911 + O(7^300))*x^3 + (1420031 + 
						                    O(7^300))*x^2 + (2813695 + O(7^300))*x + 3121458 + O(7^300)

						                 NumberOfLines = 2    


						                 ----------------------------------------------------
								----------------------------------------------------

						We compute the number of lines of a random cubic surface distributed using the Tropical measure where the coefficients random powers of 7 as follows:		
								
								----------------------------------------------------
								----------------------------------------------------
								-- Example 3: Tropical Measure:
						                 -- input:
						                 f:= randomSmoothCubicSurface(7,7,powers());
						                 NumberOfLines:= howManyLines(f,7);

						                 --output:
						                 f = 823543*x^3 + 117649*x^2*y + 16807*x*y^2 + y^3 + x^2*z + 7*x*y*z + 117649*y^2*z + x*z^2 + 16807*y*z^2 + 823543*z^3 + 7*x^2*w + 
						                    x*y*w + y^2*w + 343*x*z*w + 7*y*z*w + 49*z^2*w + 343*x*w^2 + 49*y*w^2 + 117649*z*w^2 + 823543*w^3

						                 NumberOfLines = 7    

								----------------------------------------------------
								----------------------------------------------------
								
								
						We compute the number of lines of a random cubic surface distributed using the Tropical generic measure where the coefficients are random powers of 7 multiplied by a random unit in `Z_p` as follows:		
								
								----------------------------------------------------
								----------------------------------------------------
								-- Example 4: Tropical Generic Measure:
						                 -- input:
						                 units:=truncated_units(7,7);
						                 f:=randomSmoothCubicSurface(7,7,powers(),units);
						                 NumberOfLines:= howManyLines(f,7);

						                 -- output:
						                 f = 179712449*x^3 + 256310866*x^2*y + 91664806562*x*y^2 + 431600*y^3 + 
						                   10408541486*x^2*z + 105907424*x*y*z + 726144034*y^2*z + 34027070*x*z^2 + 
						                   51636734345*y*z^2 + 49606032*z^3 + 1799042889*x^2*w + 133144667439*x*y*w + 
						                   12002450138*y^2*w + 223006385427*x*z*w + 388129227556*y*z*w + 
						                   261702140*z^2*w + 641675881595*x*w^2 + 18186770665*y*w^2 + 59408629*z*w^2 + 
						                   280603498*w^3

						                 NumberOfLines = 0   
								----------------------------------------------------
								----------------------------------------------------


						We create a smooth cubic whose coefficients are sampled uniformly random from the Gaussian distribution with standard deviation 0.3 and determine the number of real lines that it contains:


								----------------------------------------------------
								----------------------------------------------------
								-- Example 5: Real case:
						                 -- input:
								f := randomSmoothCubicSurface(0.3);
								NumberOfLines:= howManyLines(f);

						                 -- output:
						                 f = 84253/100000*x^3 - 5127/10000*x^2*y - 229151/200000*x*y^2 - 7141/6250*y^3 - 
						  		 30111/25000*x^2*z - 6707/100000*x*y*z - 1551/2500*y^2*z - 
						  		 211107/200000*x*z^2 - 57147/100000*y*z^2 + 9807/100000*z^3 + 
						  		 56707/100000*x^2*w + 32001/50000*x*y*w - 1333/100000*y^2*w + 
						  		 4011/12500*x*z*w - 2347/20000*y*z*w - 237/6250*z^2*w - 16147/12500*x*w^2 + 
						  		 22329/100000*y*w^2 - 81351/100000*z*w^2 - 33261/50000*w^3

						                 NumberOfLines = 3   
								----------------------------------------------------
								----------------------------------------------------
							       
