partitionCounts := [[[6], 					[0]	 ],
					[[5,1], 				[2]	 ],
					[[4, 2 ], 				[1]	 ],
					[[4, 1, 1], 			[5]	 ],
					[[3, 3], 				[0]	 ],
					[[3, 2, 1], 			[3]	 ],
					[[3, 1, 1, 1], 			[9]	 ],
					[[2, 2, 2], 			[3]	 ],
					[[2, 2, 1, 1], 			[7]	 ],
					[[2, 1, 1, 1, 1], 		[15] ],
					[[1, 1, 1, 1, 1, 1], 	[27]]];



function randomPolynomial(p,N)

	K := pAdicField(p,300);
	_<x> := PolynomialRing(K);
	
	listCoef := [0..p^(N+1)-1];	

    randomPoly:= 0*x;

	for i in  [0..6] do
		coef := Random(listCoef);
		randomPoly :=  randomPoly +  coef * x^i;
	end for;

	return randomPoly;

end function;



function howManyLines(g)

		c := -1;

		try  F := Factorization(g);

		part := Reverse( Sort([Degree(factor[1]) : factor in F ]) );

		for j in partitionCounts do
			if j[1] eq part then
				c := j[2][1];
			end if;
		end for;

		catch e c := -1;
		end try;

		return c;

end function;



function lineCounts(p,N,M)

	counts := [0 : j in [-1..27]];

	c := -1;

	for i in [1..M] do

		if (i mod 1000) eq 0 then 
			print i; 
		end if;

		g := randomPolynomial(p,N);
		c := howManyLines(g);

		counts[c+2] := counts[c+2] + 1;

		while c eq -1 do
			g := randomPolynomial(p,N);
			c := howManyLines(g);
			counts[c+2] := counts[c+2] + 1;
		end while;

	end for;

	return counts;

end function;

///////////////////////////////////////////
////////////////// TESTS //////////////////
///////////////////////////////////////////

p := 7; // prime
M := 1000; // number of trials
N := 7; // precision

counts := lineCounts(p,N,M);

RR := RealField(5);

print("--------------------------------------------------------------------------------------------");
print "Measure: BlowUp  |  Prime: ",p," |   Number of trials: ",M,"|  Range of coeffs: [0,p^",N+1,")";
print("--------------------------------------------------------------------------------------------");

for j in [-1,0,1,2,3,5,7,9,15,27] do
	print j," lines: ", RR!(counts[j+2]/M);
end for;
