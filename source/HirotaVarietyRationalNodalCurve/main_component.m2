restart
C = ZZ/32999

g = 6
r = 2^g+3*g --number parameters in product space  
t = 2*g+g+1 --number of kappas and lambdas
p = 2^g-1     --number of a's
--the elements of the cube were generated using a function in Mathematica
--cube for g=6
cube = {{0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 1}, {0, 0, 0, 0, 1, 0}, {0, 0, 0,
   0, 1, 1}, {0, 0, 0, 1, 0, 0}, {0, 0, 0, 1, 0, 1}, {0, 0, 0, 1, 1, 
  0}, {0, 0, 0, 1, 1, 1}, {0, 0, 1, 0, 0, 0}, {0, 0, 1, 0, 0, 1}, {0, 
  0, 1, 0, 1, 0}, {0, 0, 1, 0, 1, 1}, {0, 0, 1, 1, 0, 0}, {0, 0, 1, 1,
   0, 1}, {0, 0, 1, 1, 1, 0}, {0, 0, 1, 1, 1, 1}, {0, 1, 0, 0, 0, 
  0}, {0, 1, 0, 0, 0, 1}, {0, 1, 0, 0, 1, 0}, {0, 1, 0, 0, 1, 1}, {0, 
  1, 0, 1, 0, 0}, {0, 1, 0, 1, 0, 1}, {0, 1, 0, 1, 1, 0}, {0, 1, 0, 1,
   1, 1}, {0, 1, 1, 0, 0, 0}, {0, 1, 1, 0, 0, 1}, {0, 1, 1, 0, 1, 
  0}, {0, 1, 1, 0, 1, 1}, {0, 1, 1, 1, 0, 0}, {0, 1, 1, 1, 0, 1}, {0, 
  1, 1, 1, 1, 0}, {0, 1, 1, 1, 1, 1}, {1, 0, 0, 0, 0, 0}, {1, 0, 0, 0,
   0, 1}, {1, 0, 0, 0, 1, 0}, {1, 0, 0, 0, 1, 1}, {1, 0, 0, 1, 0, 
  0}, {1, 0, 0, 1, 0, 1}, {1, 0, 0, 1, 1, 0}, {1, 0, 0, 1, 1, 1}, {1, 
  0, 1, 0, 0, 0}, {1, 0, 1, 0, 0, 1}, {1, 0, 1, 0, 1, 0}, {1, 0, 1, 0,
   1, 1}, {1, 0, 1, 1, 0, 0}, {1, 0, 1, 1, 0, 1}, {1, 0, 1, 1, 1, 
  0}, {1, 0, 1, 1, 1, 1}, {1, 1, 0, 0, 0, 0}, {1, 1, 0, 0, 0, 1}, {1, 
  1, 0, 0, 1, 0}, {1, 1, 0, 0, 1, 1}, {1, 1, 0, 1, 0, 0}, {1, 1, 0, 1,
   0, 1}, {1, 1, 0, 1, 1, 0}, {1, 1, 0, 1, 1, 1}, {1, 1, 1, 0, 0, 
  0}, {1, 1, 1, 0, 0, 1}, {1, 1, 1, 0, 1, 0}, {1, 1, 1, 0, 1, 1}, {1, 
  1, 1, 1, 0, 0}, {1, 1, 1, 1, 0, 1}, {1, 1, 1, 1, 1, 0}, {1, 1, 1, 1,
   1, 1}};

  
--create the a generators
agens = {};
for i from 0 to p do 
    agens=append(agens,a_(cube_i))
#agens == 2^g
--create the vectors of the degrees 
deg = {}; 
for i from 1 to g do
    deg = append(deg,1)
for i from 1 to g do
    deg = append(deg,2)
for i from 1 to g do
    deg = append(deg,3)
for i from 1 to p+1 do
    deg = append(deg,0)
--    
R = C[u_1..u_g,v_1..v_g,w_1..w_g,agens,Degrees=>deg];
#(generators R)==r
kappas=2*g
lambdas=g
T = C[k_1..k_kappas,l_0..l_lambdas];
t == #(generators T)
S = R[q_1..q_g];
--extract a generators
Agens = {};
for i from 3*g to r-1 do
    Agens = append(Agens,(generators R)_i)
#Agens==2^g
--
l = 0;
for i from 1 to g do
    l = l + q_i^2
I = ideal(l);
G = S/I;
--now we write the degenerate theta function
F = 0;
mon = 1;
--cubeS = sub(cube,S);
-*for i from 0 to p do {    --index moving elements of cube
    mon = 1;
    for j from 0 to g-1 do {    --index moving elements in the vertex of the cube
	if (cube_i)_j == 1 then 
	   mon = mon * q_(j+1)};	   
    F = F + a_(cube_i)*mon;
}*-
for i from 0 to p do {    --index moving elements of cube
    mon = 1;
    for j from 0 to g-1 do {    --index moving elements in the vertex of the cube
	if (cube_i)_j == 1 then 
	   mon = mon * q_(j+1)};	   
    F = F + Agens_i*mon;
}
F
use S


dx = f -> u_1*q_1*diff(q_1,f)+u_2*q_2*diff(q_2,f)+u_3*q_3*diff(q_3,f)+
u_4*q_4*diff(q_4,f)+u_5*q_5*diff(q_5,f)+u_6*q_6*diff(q_6,f); 
dy = f -> v_1*q_1*diff(q_1,f)+v_2*q_2*diff(q_2,f)+v_3*q_3*diff(q_3,f)+
v_4*q_4*diff(q_4,f)+v_5*q_5*diff(q_5,f)+v_6*q_6*diff(q_6,f); 
dt = f -> w_1*q_1*diff(q_1,f)+w_2*q_2*diff(q_2,f)+w_3*q_3*diff(q_3,f)+
w_4*q_4*diff(q_4,f)+w_5*q_5*diff(q_5,f)+w_6*q_6*diff(q_6,f);
 


FX = dx(F);
FXX = dx(FX);
FXXX = dx(FXX);
FXXXX = dx(FXXX);
FY = dy(F);
FYY = dy(FY);
FT = dt(F);
FXT = dx(FT);
-- Now we impose Hirota's bilinear relation 

hirota = FXXXX*F-4*FXXX*FX+3*FXX^2+4*FX*FT-4*FXT*F+3*FYY*F-3*FY^2;
 -- We impose that all coefficients vanish and we get the condtion that we are looking for.
H = sub(ideal((coefficients(hirota))_1),R);
H = ideal mingens H;

numgens H

J = jacobian(H);


--we create the list of indeces appearing in the kappas in the parametrization for each vertex of the cube
I = mutableMatrix(ZZ,g,2^g);
for k from 0 to p do {
    for i from 0 to g-1 do{
    	if i == 0 then {
       	    if (cube_k)_i == 0 then
               I_(i,k) = 2
            else 
               I_(i,k) = 1}
    	else 
       	    if (cube_k)_i == 0 then
                I_(i,k) = 2*i+2
            else 
               I_(i,k) = 2*i+1	         
   }
}


--we create a matrix whose entries are the parametrization for the a_i's
param = mutableMatrix(C,1,p+1);
--initialize the matrix param to have all 1's as entries
for i from 0 to 0 do { 
    for j from 0 to p do         
        param_(i,j) = 1 
    }
-- vector of random entries for the kappa parameters
kk={};
for i from 0 to kappas-1 do 
    kk = append(kk,random(C));
#kk == kappas
for i from 0 to 0 do { 
    for j from 0 to p do {
	for t from 0 to g-2 do { 
	    for s from t+1 to g-1 do
            param_(i,j)=param_(i,j)*(kk_(I_(t,j)-1) - kk_(I_(s,j)-1))         
        }
    }   
}

--parametrization for u,v,w
Puvw = {};
for i from 1 to kappas-1 do {
        if i%2==1 then
	Puvw = append(Puvw,kk_(i-1)-kk_(i));
}
for i from 1 to kappas-1 do {
        if i%2==1 then
	Puvw = append(Puvw,kk_(i-1)^2-kk_(i)^2);
}
for i from 1 to kappas-1 do {
        if i%2==1 then
	Puvw = append(Puvw,kk_(i-1)^3-kk_(i)^3);
}

G = mutableMatrix(C,1,r);
for i from 0 to 0 do {
    for j from 0 to 3*g-1 do 
        G_(i,j) = Puvw_j;
    for j from 3*g to r-1 do
    	G_(i,j) = param_(0,j-3*g)
    	}
G = matrix G;


f = map(C,R,G);


expectedRank = r-(3*g+1)
tang = f(J);
rank tang 
 