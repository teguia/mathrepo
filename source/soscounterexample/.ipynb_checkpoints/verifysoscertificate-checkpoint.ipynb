{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## An SOS counterexample to an inequality of symmetric functions\n",
    "\n",
    "This document verifies the fact that a certain symmetric polynomial can be written as a sum of squares. This provides a counterexample to a certain conjecture relating nonnegativity and the dominance (or majorization) partial order on partitions. See https://arxiv.org/abs/1909.00081 for more information. Our symmetry-adapted SDP returned a numerical matrix (with floating point entries). Since we had reduced the problem size using representation theory of the symmetric group, and also knowledge of the real zeros of our polynomial, we were able to successfully round the floating point entries into (exact) rational numbers.\n",
    "\n",
    "We load this matrix $A$, whose entries are rational numbers, as well as a permutation matrix which reorders the columns and rows so that we can apply naive $LDL^T$ decomposition below. In order to apply $LDL^T$ decomposition, it is sometimes required to reorder the rows and columns in order to avoid zero pivots."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "P = load(\"counterexample-P.sobj\")\n",
    "A = load(\"counterexample-A.sobj\")\n",
    "B = P.T * A * P\n",
    "C = B.change_ring(QQ)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below we apply $LDL^T$ decomposition to extract pairs $(d,\\ell)$ where $d$ is the pivot entry and $\\ell$ is a vector. These pairs can be used to write $$C = \\sum d \\cdot \\ell \\ell^T $$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def naiveLDLt(A):\n",
    "    N = A.nrows()\n",
    "    terms = [] # (d,l) pair of diagonal entry and vector\n",
    "    for i in range(0,N):\n",
    "        pivot = A[i,i]\n",
    "        if pivot != 0:\n",
    "            l = 1/pivot * vector(list(A.columns()[i]))\n",
    "            terms.append((pivot,l))\n",
    "            A = A - pivot * l.outer_product(l)\n",
    "    return terms\n",
    "\n",
    "terms = naiveLDLt(C)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below we create a vector of monomials with total degree $8$ in three variables. The permutation matrix $P$ loaded above reorders this basis appropriately to match the reordering of the matrix $A$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\left(x_{1}^{3} x_{2}^{3} x_{3}^{2},\\,x_{1}^{3} x_{2}^{2} x_{3}^{3},\\,x_{1}^{2} x_{2}^{3} x_{3}^{3},\\,x_{1}^{4} x_{2}^{2} x_{3}^{2},\\,x_{2}^{4} x_{3}^{4},\\,x_{1}^{4} x_{2}^{4},\\,x_{1}^{4} x_{3}^{4},\\,x_{1}^{4} x_{2}^{3} x_{3},\\,x_{1}^{3} x_{2}^{4} x_{3},\\,x_{1}^{3} x_{2} x_{3}^{4},\\,x_{1}^{4} x_{2} x_{3}^{3},\\,x_{1} x_{2}^{4} x_{3}^{3},\\,x_{1} x_{2}^{3} x_{3}^{4},\\,x_{1}^{2} x_{2}^{4} x_{3}^{2},\\,x_{1}^{5} x_{2}^{3},\\,x_{1}^{5} x_{3}^{3},\\,x_{2}^{5} x_{3}^{3},\\,x_{1}^{2} x_{2}^{2} x_{3}^{4},\\,x_{1}^{3} x_{2}^{5},\\,x_{1}^{3} x_{3}^{5},\\,x_{2}^{3} x_{3}^{5},\\,x_{1}^{2} x_{2}^{5} x_{3},\\,x_{1} x_{2}^{5} x_{3}^{2},\\,x_{1}^{5} x_{2}^{2} x_{3},\\,x_{1}^{2} x_{3}^{6},\\,x_{1}^{2} x_{2} x_{3}^{5},\\,x_{1} x_{2}^{2} x_{3}^{5},\\,x_{1}^{5} x_{2} x_{3}^{2},\\,x_{1}^{2} x_{2}^{6},\\,x_{2}^{6} x_{3}^{2},\\,x_{1}^{8},\\,x_{1} x_{2} x_{3}^{6},\\,x_{1} x_{2}^{6} x_{3},\\,x_{1}^{6} x_{2} x_{3},\\,x_{3}^{8},\\,x_{2}^{8},\\,x_{1} x_{3}^{7},\\,x_{2}^{7} x_{3},\\,x_{1} x_{2}^{7},\\,x_{1}^{6} x_{3}^{2},\\,x_{1}^{6} x_{2}^{2},\\,x_{1}^{7} x_{2},\\,x_{2}^{2} x_{3}^{6},\\,x_{2} x_{3}^{7},\\,x_{1}^{7} x_{3}\\right)</script></html>"
      ],
      "text/latex": [
       "\\begin{math}\n",
       "\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\left(x_{1}^{3} x_{2}^{3} x_{3}^{2},\\,x_{1}^{3} x_{2}^{2} x_{3}^{3},\\,x_{1}^{2} x_{2}^{3} x_{3}^{3},\\,x_{1}^{4} x_{2}^{2} x_{3}^{2},\\,x_{2}^{4} x_{3}^{4},\\,x_{1}^{4} x_{2}^{4},\\,x_{1}^{4} x_{3}^{4},\\,x_{1}^{4} x_{2}^{3} x_{3},\\,x_{1}^{3} x_{2}^{4} x_{3},\\,x_{1}^{3} x_{2} x_{3}^{4},\\,x_{1}^{4} x_{2} x_{3}^{3},\\,x_{1} x_{2}^{4} x_{3}^{3},\\,x_{1} x_{2}^{3} x_{3}^{4},\\,x_{1}^{2} x_{2}^{4} x_{3}^{2},\\,x_{1}^{5} x_{2}^{3},\\,x_{1}^{5} x_{3}^{3},\\,x_{2}^{5} x_{3}^{3},\\,x_{1}^{2} x_{2}^{2} x_{3}^{4},\\,x_{1}^{3} x_{2}^{5},\\,x_{1}^{3} x_{3}^{5},\\,x_{2}^{3} x_{3}^{5},\\,x_{1}^{2} x_{2}^{5} x_{3},\\,x_{1} x_{2}^{5} x_{3}^{2},\\,x_{1}^{5} x_{2}^{2} x_{3},\\,x_{1}^{2} x_{3}^{6},\\,x_{1}^{2} x_{2} x_{3}^{5},\\,x_{1} x_{2}^{2} x_{3}^{5},\\,x_{1}^{5} x_{2} x_{3}^{2},\\,x_{1}^{2} x_{2}^{6},\\,x_{2}^{6} x_{3}^{2},\\,x_{1}^{8},\\,x_{1} x_{2} x_{3}^{6},\\,x_{1} x_{2}^{6} x_{3},\\,x_{1}^{6} x_{2} x_{3},\\,x_{3}^{8},\\,x_{2}^{8},\\,x_{1} x_{3}^{7},\\,x_{2}^{7} x_{3},\\,x_{1} x_{2}^{7},\\,x_{1}^{6} x_{3}^{2},\\,x_{1}^{6} x_{2}^{2},\\,x_{1}^{7} x_{2},\\,x_{2}^{2} x_{3}^{6},\\,x_{2} x_{3}^{7},\\,x_{1}^{7} x_{3}\\right)\n",
       "\\end{math}"
      ],
      "text/plain": [
       "(x1^3*x2^3*x3^2, x1^3*x2^2*x3^3, x1^2*x2^3*x3^3, x1^4*x2^2*x3^2, x2^4*x3^4, x1^4*x2^4, x1^4*x3^4, x1^4*x2^3*x3, x1^3*x2^4*x3, x1^3*x2*x3^4, x1^4*x2*x3^3, x1*x2^4*x3^3, x1*x2^3*x3^4, x1^2*x2^4*x3^2, x1^5*x2^3, x1^5*x3^3, x2^5*x3^3, x1^2*x2^2*x3^4, x1^3*x2^5, x1^3*x3^5, x2^3*x3^5, x1^2*x2^5*x3, x1*x2^5*x3^2, x1^5*x2^2*x3, x1^2*x3^6, x1^2*x2*x3^5, x1*x2^2*x3^5, x1^5*x2*x3^2, x1^2*x2^6, x2^6*x3^2, x1^8, x1*x2*x3^6, x1*x2^6*x3, x1^6*x2*x3, x3^8, x2^8, x1*x3^7, x2^7*x3, x1*x2^7, x1^6*x3^2, x1^6*x2^2, x1^7*x2, x2^2*x3^6, x2*x3^7, x1^7*x3)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "def create_monomials(n,d):\n",
    "    varz = [var('x%s'%i) for i in range(1,n+1)]\n",
    "    mons = []\n",
    "    for exps in IntegerVectors(d,n):\n",
    "        prod = varz[0] - varz[0] + 1\n",
    "        for i,x in enumerate(varz):\n",
    "            prod *= x^exps[i]\n",
    "        mons.append(prod)\n",
    "    return mons\n",
    "\n",
    "m = create_monomials(3,8)\n",
    "m_shuffled = P.T * vector(m)\n",
    "show(m_shuffled)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below we sum the squares. We use each vector $\\ell$ to create a polynomial $\\ell m(x)$, which we square and sum with positive coefficient $d$. The resulting polynomial displayed below is therefore a sum of squares with positive coefficients."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\mathrm{True}</script></html>"
      ],
      "text/latex": [
       "\\begin{math}\n",
       "\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\mathrm{True}\n",
       "\\end{math}"
      ],
      "text/plain": [
       "True"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "okay = True\n",
    "for tup in terms:\n",
    "    d,l = tup[0],tup[1]\n",
    "    if d < 0:\n",
    "        okay = False\n",
    "show(okay)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "17/9450*x1^16 + 1/1050*x1^14*x2^2 + 1/9450*x1^12*x2^4 + 1/525*x1^10*x2^6 + 2/315*x1^8*x2^8 + 1/525*x1^6*x2^10 + 1/9450*x1^4*x2^12 + 1/1050*x1^2*x2^14 + 17/9450*x2^16 + 1/1050*x1^14*x3^2 - 16/4725*x1^12*x2^2*x3^2 - 8/1575*x1^10*x2^4*x3^2 + 11/9450*x1^8*x2^6*x3^2 + 11/9450*x1^6*x2^8*x3^2 - 8/1575*x1^4*x2^10*x3^2 - 16/4725*x1^2*x2^12*x3^2 + 1/1050*x2^14*x3^2 + 1/9450*x1^12*x3^4 - 8/1575*x1^10*x2^2*x3^4 - 11/4725*x1^8*x2^4*x3^4 - 1/1890*x1^6*x2^6*x3^4 - 11/4725*x1^4*x2^8*x3^4 - 8/1575*x1^2*x2^10*x3^4 + 1/9450*x2^12*x3^4 + 1/525*x1^10*x3^6 + 11/9450*x1^8*x2^2*x3^6 - 1/1890*x1^6*x2^4*x3^6 - 1/1890*x1^4*x2^6*x3^6 + 11/9450*x1^2*x2^8*x3^6 + 1/525*x2^10*x3^6 + 2/315*x1^8*x3^8 + 11/9450*x1^6*x2^2*x3^8 - 11/4725*x1^4*x2^4*x3^8 + 11/9450*x1^2*x2^6*x3^8 + 2/315*x2^8*x3^8 + 1/525*x1^6*x3^10 - 8/1575*x1^4*x2^2*x3^10 - 8/1575*x1^2*x2^4*x3^10 + 1/525*x2^6*x3^10 + 1/9450*x1^4*x3^12 - 16/4725*x1^2*x2^2*x3^12 + 1/9450*x2^4*x3^12 + 1/1050*x1^2*x3^14 + 1/1050*x2^2*x3^14 + 17/9450*x3^16\n"
     ]
    },
    {
     "ename": "RuntimeError",
     "evalue": "ECL says: The function SET-LOCALE-SUBDIR is undefined.",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mRuntimeError\u001b[0m                              Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-12-07c7831c51eb>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[1;32m      8\u001b[0m \u001b[0mres\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0msum\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mans\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      9\u001b[0m \u001b[0mprint\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mres\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 10\u001b[0;31m \u001b[0mres\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0msimplify\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;32m/Applications/SageMath-9.2.app/Contents/Resources/sage/local/lib/python3.8/site-packages/sage/symbolic/expression.pyx\u001b[0m in \u001b[0;36msage.symbolic.expression.Expression.simplify (build/cythonized/sage/symbolic/expression.cpp:53133)\u001b[0;34m()\u001b[0m\n\u001b[1;32m   9992\u001b[0m             \u001b[0msage\u001b[0m\u001b[0;34m:\u001b[0m \u001b[0mforget\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m   9993\u001b[0m         \"\"\"\n\u001b[0;32m-> 9994\u001b[0;31m         \u001b[0;32mreturn\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_parent\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_maxima_\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m   9995\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m   9996\u001b[0m     \u001b[0;32mdef\u001b[0m \u001b[0msimplify_full\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/Applications/SageMath-9.2.app/Contents/Resources/sage/local/lib/python3.8/site-packages/sage/symbolic/expression.pyx\u001b[0m in \u001b[0;36msage.symbolic.expression.Expression._maxima_ (build/cythonized/sage/symbolic/expression.cpp:7934)\u001b[0;34m()\u001b[0m\n\u001b[1;32m   1013\u001b[0m             \u001b[0;31m# Maybe not such a great idea because the \"default\" interface is another one\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m   1014\u001b[0m             \u001b[0;32mfrom\u001b[0m \u001b[0msage\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mcalculus\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mcalculus\u001b[0m \u001b[0;32mimport\u001b[0m \u001b[0mmaxima\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m-> 1015\u001b[0;31m             \u001b[0;32mreturn\u001b[0m \u001b[0msuper\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mExpression\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_interface_\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mmaxima\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m   1016\u001b[0m         \u001b[0;32melse\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m   1017\u001b[0m             \u001b[0;32mreturn\u001b[0m \u001b[0msuper\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mExpression\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_interface_\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0msession\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/Applications/SageMath-9.2.app/Contents/Resources/sage/local/lib/python3.8/site-packages/sage/structure/sage_object.pyx\u001b[0m in \u001b[0;36msage.structure.sage_object.SageObject._interface_ (build/cythonized/sage/structure/sage_object.c:5481)\u001b[0;34m()\u001b[0m\n\u001b[1;32m    678\u001b[0m             \u001b[0;32mexcept\u001b[0m \u001b[0;34m(\u001b[0m\u001b[0mKeyError\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mValueError\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    679\u001b[0m                 \u001b[0;32mpass\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 680\u001b[0;31m         \u001b[0mnm\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mI\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mname\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    681\u001b[0m         \u001b[0minit_func\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mgetattr\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34m'_%s_init_'\u001b[0m \u001b[0;34m%\u001b[0m \u001b[0mnm\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;32mNone\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    682\u001b[0m         \u001b[0;32mif\u001b[0m \u001b[0minit_func\u001b[0m \u001b[0;32mis\u001b[0m \u001b[0;32mnot\u001b[0m \u001b[0;32mNone\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/Applications/SageMath-9.2.app/Contents/Resources/sage/local/lib/python3.8/site-packages/sage/misc/lazy_import.pyx\u001b[0m in \u001b[0;36msage.misc.lazy_import.LazyImport.__getattr__ (build/cythonized/sage/misc/lazy_import.c:3871)\u001b[0;34m()\u001b[0m\n\u001b[1;32m    327\u001b[0m             \u001b[0;32mTrue\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    328\u001b[0m         \"\"\"\n\u001b[0;32m--> 329\u001b[0;31m         \u001b[0;32mreturn\u001b[0m \u001b[0mgetattr\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mget_object\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mattr\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    330\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    331\u001b[0m     \u001b[0;31m# We need to wrap all the slot methods, as they are not forwarded\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/Applications/SageMath-9.2.app/Contents/Resources/sage/local/lib/python3.8/site-packages/sage/misc/lazy_import.pyx\u001b[0m in \u001b[0;36msage.misc.lazy_import.LazyImport.get_object (build/cythonized/sage/misc/lazy_import.c:2436)\u001b[0;34m()\u001b[0m\n\u001b[1;32m    189\u001b[0m         \u001b[0;32mif\u001b[0m \u001b[0mlikely\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_object\u001b[0m \u001b[0;32mis\u001b[0m \u001b[0;32mnot\u001b[0m \u001b[0;32mNone\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    190\u001b[0m             \u001b[0;32mreturn\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_object\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 191\u001b[0;31m         \u001b[0;32mreturn\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_get_object\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    192\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    193\u001b[0m     \u001b[0mcpdef\u001b[0m \u001b[0m_get_object\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/Applications/SageMath-9.2.app/Contents/Resources/sage/local/lib/python3.8/site-packages/sage/misc/lazy_import.pyx\u001b[0m in \u001b[0;36msage.misc.lazy_import.LazyImport._get_object (build/cythonized/sage/misc/lazy_import.c:2705)\u001b[0;34m()\u001b[0m\n\u001b[1;32m    222\u001b[0m             \u001b[0mprint\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m'Option ``at_startup=True`` for lazy import {0} not needed anymore'\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mformat\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_name\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    223\u001b[0m         \u001b[0;32mtry\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 224\u001b[0;31m             \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_object\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mgetattr\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0m__import__\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_module\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34m{\u001b[0m\u001b[0;34m}\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34m{\u001b[0m\u001b[0;34m}\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34m[\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_name\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_name\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    225\u001b[0m         \u001b[0;32mexcept\u001b[0m \u001b[0mImportError\u001b[0m \u001b[0;32mas\u001b[0m \u001b[0me\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    226\u001b[0m             \u001b[0;32mif\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_feature\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/Applications/SageMath-9.2.app/Contents/Resources/sage/local/lib/python3.8/site-packages/sage/interfaces/maxima_lib.py\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[1;32m    109\u001b[0m \u001b[0mecl_eval\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m\"(setq $nolabels t))\"\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    110\u001b[0m \u001b[0mecl_eval\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m\"(defvar *MAXIMA-LANG-SUBDIR* NIL)\"\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 111\u001b[0;31m \u001b[0mecl_eval\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m\"(set-locale-subdir)\"\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    112\u001b[0m \u001b[0mecl_eval\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m\"(set-pathnames)\"\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    113\u001b[0m \u001b[0mecl_eval\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m\"(defun add-lineinfo (x) x)\"\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/Applications/SageMath-9.2.app/Contents/Resources/sage/local/lib/python3.8/site-packages/sage/libs/ecl.pyx\u001b[0m in \u001b[0;36msage.libs.ecl.ecl_eval (build/cythonized/sage/libs/ecl.c:11009)\u001b[0;34m()\u001b[0m\n\u001b[1;32m   1370\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m   1371\u001b[0m \u001b[0;31m#convenience routine to more easily evaluate strings\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m-> 1372\u001b[0;31m \u001b[0mcpdef\u001b[0m \u001b[0mEclObject\u001b[0m \u001b[0mecl_eval\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mstr\u001b[0m \u001b[0ms\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m   1373\u001b[0m     r\"\"\"\n\u001b[1;32m   1374\u001b[0m     \u001b[0mRead\u001b[0m \u001b[0;32mand\u001b[0m \u001b[0mevaluate\u001b[0m \u001b[0mstring\u001b[0m \u001b[0;32min\u001b[0m \u001b[0mLisp\u001b[0m \u001b[0;32mand\u001b[0m \u001b[0;32mreturn\u001b[0m \u001b[0mthe\u001b[0m \u001b[0mresult\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/Applications/SageMath-9.2.app/Contents/Resources/sage/local/lib/python3.8/site-packages/sage/libs/ecl.pyx\u001b[0m in \u001b[0;36msage.libs.ecl.ecl_eval (build/cythonized/sage/libs/ecl.c:10942)\u001b[0;34m()\u001b[0m\n\u001b[1;32m   1393\u001b[0m     \"\"\"\n\u001b[1;32m   1394\u001b[0m     \u001b[0mcdef\u001b[0m \u001b[0mcl_object\u001b[0m \u001b[0mo\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m-> 1395\u001b[0;31m     \u001b[0mo\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mecl_safe_eval\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mpython_to_ecl\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0ms\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;32mTrue\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m   1396\u001b[0m     \u001b[0;32mreturn\u001b[0m \u001b[0mecl_wrap\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mo\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m   1397\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/Applications/SageMath-9.2.app/Contents/Resources/sage/local/lib/python3.8/site-packages/sage/libs/ecl.pyx\u001b[0m in \u001b[0;36msage.libs.ecl.ecl_safe_eval (build/cythonized/sage/libs/ecl.c:5574)\u001b[0;34m()\u001b[0m\n\u001b[1;32m    337\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    338\u001b[0m     \u001b[0;32mif\u001b[0m \u001b[0merror\u001b[0m \u001b[0;34m!=\u001b[0m \u001b[0mNULL\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 339\u001b[0;31m         raise RuntimeError(\"ECL says: {}\".format(\n\u001b[0m\u001b[1;32m    340\u001b[0m             ecl_string_to_python(error)))\n\u001b[1;32m    341\u001b[0m     \u001b[0;32melse\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mRuntimeError\u001b[0m: ECL says: The function SET-LOCALE-SUBDIR is undefined."
     ]
    }
   ],
   "source": [
    "ans = []\n",
    "for tup in terms:\n",
    "    d,l = tup[0],tup[1]\n",
    "    term = d * (l*m_shuffled)^2\n",
    "    term = term.expand()\n",
    "    ans.append(term)\n",
    "\n",
    "res = sum(ans)\n",
    "print(res)\n",
    "res.simplify()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below we create the symmetric polynomial $H_{44} - H_{521}$, evaluated at $x_1^2, x_2^2, x_3^2$ to check nonnegativity on the nonnegative orthant, as explained in https://arxiv.org/abs/1909.00081. The nonnegativity of this polynomial provides the counterexample to the conjecture."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{17}{9450} \\, x_{1}^{16} + \\frac{1}{1050} \\, x_{1}^{14} x_{2}^{2} + \\frac{1}{9450} \\, x_{1}^{12} x_{2}^{4} + \\frac{1}{525} \\, x_{1}^{10} x_{2}^{6} + \\frac{2}{315} \\, x_{1}^{8} x_{2}^{8} + \\frac{1}{525} \\, x_{1}^{6} x_{2}^{10} + \\frac{1}{9450} \\, x_{1}^{4} x_{2}^{12} + \\frac{1}{1050} \\, x_{1}^{2} x_{2}^{14} + \\frac{17}{9450} \\, x_{2}^{16} + \\frac{1}{1050} \\, x_{1}^{14} x_{3}^{2} - \\frac{16}{4725} \\, x_{1}^{12} x_{2}^{2} x_{3}^{2} - \\frac{8}{1575} \\, x_{1}^{10} x_{2}^{4} x_{3}^{2} + \\frac{11}{9450} \\, x_{1}^{8} x_{2}^{6} x_{3}^{2} + \\frac{11}{9450} \\, x_{1}^{6} x_{2}^{8} x_{3}^{2} - \\frac{8}{1575} \\, x_{1}^{4} x_{2}^{10} x_{3}^{2} - \\frac{16}{4725} \\, x_{1}^{2} x_{2}^{12} x_{3}^{2} + \\frac{1}{1050} \\, x_{2}^{14} x_{3}^{2} + \\frac{1}{9450} \\, x_{1}^{12} x_{3}^{4} - \\frac{8}{1575} \\, x_{1}^{10} x_{2}^{2} x_{3}^{4} - \\frac{11}{4725} \\, x_{1}^{8} x_{2}^{4} x_{3}^{4} - \\frac{1}{1890} \\, x_{1}^{6} x_{2}^{6} x_{3}^{4} - \\frac{11}{4725} \\, x_{1}^{4} x_{2}^{8} x_{3}^{4} - \\frac{8}{1575} \\, x_{1}^{2} x_{2}^{10} x_{3}^{4} + \\frac{1}{9450} \\, x_{2}^{12} x_{3}^{4} + \\frac{1}{525} \\, x_{1}^{10} x_{3}^{6} + \\frac{11}{9450} \\, x_{1}^{8} x_{2}^{2} x_{3}^{6} - \\frac{1}{1890} \\, x_{1}^{6} x_{2}^{4} x_{3}^{6} - \\frac{1}{1890} \\, x_{1}^{4} x_{2}^{6} x_{3}^{6} + \\frac{11}{9450} \\, x_{1}^{2} x_{2}^{8} x_{3}^{6} + \\frac{1}{525} \\, x_{2}^{10} x_{3}^{6} + \\frac{2}{315} \\, x_{1}^{8} x_{3}^{8} + \\frac{11}{9450} \\, x_{1}^{6} x_{2}^{2} x_{3}^{8} - \\frac{11}{4725} \\, x_{1}^{4} x_{2}^{4} x_{3}^{8} + \\frac{11}{9450} \\, x_{1}^{2} x_{2}^{6} x_{3}^{8} + \\frac{2}{315} \\, x_{2}^{8} x_{3}^{8} + \\frac{1}{525} \\, x_{1}^{6} x_{3}^{10} - \\frac{8}{1575} \\, x_{1}^{4} x_{2}^{2} x_{3}^{10} - \\frac{8}{1575} \\, x_{1}^{2} x_{2}^{4} x_{3}^{10} + \\frac{1}{525} \\, x_{2}^{6} x_{3}^{10} + \\frac{1}{9450} \\, x_{1}^{4} x_{3}^{12} - \\frac{16}{4725} \\, x_{1}^{2} x_{2}^{2} x_{3}^{12} + \\frac{1}{9450} \\, x_{2}^{4} x_{3}^{12} + \\frac{1}{1050} \\, x_{1}^{2} x_{3}^{14} + \\frac{1}{1050} \\, x_{2}^{2} x_{3}^{14} + \\frac{17}{9450} \\, x_{3}^{16}</script></html>"
      ],
      "text/latex": [
       "\\begin{math}\n",
       "\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{17}{9450} \\, x_{1}^{16} + \\frac{1}{1050} \\, x_{1}^{14} x_{2}^{2} + \\frac{1}{9450} \\, x_{1}^{12} x_{2}^{4} + \\frac{1}{525} \\, x_{1}^{10} x_{2}^{6} + \\frac{2}{315} \\, x_{1}^{8} x_{2}^{8} + \\frac{1}{525} \\, x_{1}^{6} x_{2}^{10} + \\frac{1}{9450} \\, x_{1}^{4} x_{2}^{12} + \\frac{1}{1050} \\, x_{1}^{2} x_{2}^{14} + \\frac{17}{9450} \\, x_{2}^{16} + \\frac{1}{1050} \\, x_{1}^{14} x_{3}^{2} - \\frac{16}{4725} \\, x_{1}^{12} x_{2}^{2} x_{3}^{2} - \\frac{8}{1575} \\, x_{1}^{10} x_{2}^{4} x_{3}^{2} + \\frac{11}{9450} \\, x_{1}^{8} x_{2}^{6} x_{3}^{2} + \\frac{11}{9450} \\, x_{1}^{6} x_{2}^{8} x_{3}^{2} - \\frac{8}{1575} \\, x_{1}^{4} x_{2}^{10} x_{3}^{2} - \\frac{16}{4725} \\, x_{1}^{2} x_{2}^{12} x_{3}^{2} + \\frac{1}{1050} \\, x_{2}^{14} x_{3}^{2} + \\frac{1}{9450} \\, x_{1}^{12} x_{3}^{4} - \\frac{8}{1575} \\, x_{1}^{10} x_{2}^{2} x_{3}^{4} - \\frac{11}{4725} \\, x_{1}^{8} x_{2}^{4} x_{3}^{4} - \\frac{1}{1890} \\, x_{1}^{6} x_{2}^{6} x_{3}^{4} - \\frac{11}{4725} \\, x_{1}^{4} x_{2}^{8} x_{3}^{4} - \\frac{8}{1575} \\, x_{1}^{2} x_{2}^{10} x_{3}^{4} + \\frac{1}{9450} \\, x_{2}^{12} x_{3}^{4} + \\frac{1}{525} \\, x_{1}^{10} x_{3}^{6} + \\frac{11}{9450} \\, x_{1}^{8} x_{2}^{2} x_{3}^{6} - \\frac{1}{1890} \\, x_{1}^{6} x_{2}^{4} x_{3}^{6} - \\frac{1}{1890} \\, x_{1}^{4} x_{2}^{6} x_{3}^{6} + \\frac{11}{9450} \\, x_{1}^{2} x_{2}^{8} x_{3}^{6} + \\frac{1}{525} \\, x_{2}^{10} x_{3}^{6} + \\frac{2}{315} \\, x_{1}^{8} x_{3}^{8} + \\frac{11}{9450} \\, x_{1}^{6} x_{2}^{2} x_{3}^{8} - \\frac{11}{4725} \\, x_{1}^{4} x_{2}^{4} x_{3}^{8} + \\frac{11}{9450} \\, x_{1}^{2} x_{2}^{6} x_{3}^{8} + \\frac{2}{315} \\, x_{2}^{8} x_{3}^{8} + \\frac{1}{525} \\, x_{1}^{6} x_{3}^{10} - \\frac{8}{1575} \\, x_{1}^{4} x_{2}^{2} x_{3}^{10} - \\frac{8}{1575} \\, x_{1}^{2} x_{2}^{4} x_{3}^{10} + \\frac{1}{525} \\, x_{2}^{6} x_{3}^{10} + \\frac{1}{9450} \\, x_{1}^{4} x_{3}^{12} - \\frac{16}{4725} \\, x_{1}^{2} x_{2}^{2} x_{3}^{12} + \\frac{1}{9450} \\, x_{2}^{4} x_{3}^{12} + \\frac{1}{1050} \\, x_{1}^{2} x_{3}^{14} + \\frac{1}{1050} \\, x_{2}^{2} x_{3}^{14} + \\frac{17}{9450} \\, x_{3}^{16}\n",
       "\\end{math}"
      ],
      "text/plain": [
       "17/9450*x1^16 + 1/1050*x1^14*x2^2 + 1/9450*x1^12*x2^4 + 1/525*x1^10*x2^6 + 2/315*x1^8*x2^8 + 1/525*x1^6*x2^10 + 1/9450*x1^4*x2^12 + 1/1050*x1^2*x2^14 + 17/9450*x2^16 + 1/1050*x1^14*x3^2 - 16/4725*x1^12*x2^2*x3^2 - 8/1575*x1^10*x2^4*x3^2 + 11/9450*x1^8*x2^6*x3^2 + 11/9450*x1^6*x2^8*x3^2 - 8/1575*x1^4*x2^10*x3^2 - 16/4725*x1^2*x2^12*x3^2 + 1/1050*x2^14*x3^2 + 1/9450*x1^12*x3^4 - 8/1575*x1^10*x2^2*x3^4 - 11/4725*x1^8*x2^4*x3^4 - 1/1890*x1^6*x2^6*x3^4 - 11/4725*x1^4*x2^8*x3^4 - 8/1575*x1^2*x2^10*x3^4 + 1/9450*x2^12*x3^4 + 1/525*x1^10*x3^6 + 11/9450*x1^8*x2^2*x3^6 - 1/1890*x1^6*x2^4*x3^6 - 1/1890*x1^4*x2^6*x3^6 + 11/9450*x1^2*x2^8*x3^6 + 1/525*x2^10*x3^6 + 2/315*x1^8*x3^8 + 11/9450*x1^6*x2^2*x3^8 - 11/4725*x1^4*x2^4*x3^8 + 11/9450*x1^2*x2^6*x3^8 + 2/315*x2^8*x3^8 + 1/525*x1^6*x3^10 - 8/1575*x1^4*x2^2*x3^10 - 8/1575*x1^2*x2^4*x3^10 + 1/525*x2^6*x3^10 + 1/9450*x1^4*x3^12 - 16/4725*x1^2*x2^2*x3^12 + 1/9450*x2^4*x3^12 + 1/1050*x1^2*x3^14 + 1/1050*x2^2*x3^14 + 17/9450*x3^16"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "def input_squares(p):\n",
    "    # input is our polynomial p, like H_(2,1) - H_(1,1,1)\n",
    "    # output will be inserting each variable squared, also making it start at x1, x2,... rather than x0,x1,...\n",
    "    current_vars = p.args() # this is a tuple of variables x0, x1, x2,... etc.\n",
    "    numvars = len(current_vars)\n",
    "    new_vars = [var('x%i'%i) for i in range(1,numvars+1)]\n",
    "    subz = {}\n",
    "    for i,vr in enumerate(current_vars):\n",
    "        subz[vr] = (new_vars[i])^2\n",
    "    ans = p.subs(subz)\n",
    "    return ans\n",
    "\n",
    "def plugin_ones(p):\n",
    "    # want to return the number/scalar that results from plugging in 1's to all variables\n",
    "    current_vars = p.args() # this is a tuple of variables x0, x1, x2,... etc.\n",
    "    numvars = len(current_vars)\n",
    "    subz = {}\n",
    "    for i,vr in enumerate(current_vars):\n",
    "        subz[vr] = 1\n",
    "    ans = p.subs(subz)\n",
    "    return ans\n",
    "\n",
    "def term_normalize(p):\n",
    "    norm = plugin_ones(p)\n",
    "    ans = p/norm\n",
    "    return ans\n",
    "\n",
    "def create_difference(la,mu,n=3):\n",
    "    h = SymmetricFunctions(QQ).h()\n",
    "    # we will return the difference h_la - h_mu\n",
    "    hla = h(la).expand(n)\n",
    "    hmu = h(mu).expand(n)\n",
    "    hla = input_squares(hla)\n",
    "    hmu = input_squares(hmu)\n",
    "    hla = term_normalize(hla)\n",
    "    hmu = term_normalize(hmu)\n",
    "    ans = hla - hmu\n",
    "    return ans\n",
    "\n",
    "show(create_difference((4,4),(5,2,1)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below we verify that our previous sum of squares reproduces the desired polynomial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "res - create_difference((4,4),(5,2,1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "SageMath 9.2",
   "language": "sage",
   "name": "sagemath"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
