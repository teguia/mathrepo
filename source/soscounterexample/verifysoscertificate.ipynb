{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## An SOS counterexample to an inequality of symmetric functions\n",
    "\n",
    "This document verifies the fact that a certain symmetric polynomial can be written as a sum of squares. This provides a counterexample to a certain conjecture relating nonnegativity and the dominance (or majorization) partial order on partitions. See https://arxiv.org/abs/1909.00081 for more information. Our symmetry-adapted SDP returned a numerical matrix (with floating point entries). Since we had reduced the problem size using representation theory of the symmetric group, and also knowledge of the real zeros of our polynomial, we were able to successfully round the floating point entries into (exact) rational numbers.\n",
    "\n",
    "We load this matrix $A$, whose entries are rational numbers, as well as a permutation matrix which reorders the columns and rows so that we can apply naive $LDL^T$ decomposition below. In order to apply $LDL^T$ decomposition, it is sometimes required to reorder the rows and columns in order to avoid zero pivots."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
   ],
   "source": [
    "P = load(\"counterexample-P.sobj\")\n",
    "A = load(\"counterexample-A.sobj\")\n",
    "B = P.T * A * P\n",
    "C = B.change_ring(QQ)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Below we apply $LDL^T$ decomposition to extract pairs $(d,\\ell)$ where $d$ is the pivot entry and $\\ell$ is a vector. These pairs can be used to write $$C = \\sum d \\cdot \\ell \\ell^T $$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
   ],
   "source": [
    "def naiveLDLt(A):\n",
    "    N = A.nrows()\n",
    "    terms = [] # (d,l) pair of diagonal entry and vector\n",
    "    for i in range(0,N):\n",
    "        pivot = A[i,i]\n",
    "        if pivot != 0:\n",
    "            l = 1/pivot * vector(list(A.columns()[i]))\n",
    "            terms.append((pivot,l))\n",
    "            A = A - pivot * l.outer_product(l)\n",
    "    return terms\n",
    "\n",
    "terms = naiveLDLt(C)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Below we create a vector of monomials with total degree $8$ in three variables. The permutation matrix $P$ loaded above reorders this basis appropriately to match the reordering of the matrix $A$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\left(x_{1}^{3} x_{2}^{3} x_{3}^{2},\\,x_{1}^{3} x_{2}^{2} x_{3}^{3},\\,x_{1}^{2} x_{2}^{3} x_{3}^{3},\\,x_{1}^{4} x_{2}^{2} x_{3}^{2},\\,x_{2}^{4} x_{3}^{4},\\,x_{1}^{4} x_{2}^{4},\\,x_{1}^{4} x_{3}^{4},\\,x_{1}^{4} x_{2}^{3} x_{3},\\,x_{1}^{3} x_{2}^{4} x_{3},\\,x_{1}^{3} x_{2} x_{3}^{4},\\,x_{1}^{4} x_{2} x_{3}^{3},\\,x_{1} x_{2}^{4} x_{3}^{3},\\,x_{1} x_{2}^{3} x_{3}^{4},\\,x_{1}^{2} x_{2}^{4} x_{3}^{2},\\,x_{1}^{5} x_{2}^{3},\\,x_{1}^{5} x_{3}^{3},\\,x_{2}^{5} x_{3}^{3},\\,x_{1}^{2} x_{2}^{2} x_{3}^{4},\\,x_{1}^{3} x_{2}^{5},\\,x_{1}^{3} x_{3}^{5},\\,x_{2}^{3} x_{3}^{5},\\,x_{1}^{2} x_{2}^{5} x_{3},\\,x_{1} x_{2}^{5} x_{3}^{2},\\,x_{1}^{5} x_{2}^{2} x_{3},\\,x_{1}^{2} x_{3}^{6},\\,x_{1}^{2} x_{2} x_{3}^{5},\\,x_{1} x_{2}^{2} x_{3}^{5},\\,x_{1}^{5} x_{2} x_{3}^{2},\\,x_{1}^{2} x_{2}^{6},\\,x_{2}^{6} x_{3}^{2},\\,x_{1}^{8},\\,x_{1} x_{2} x_{3}^{6},\\,x_{1} x_{2}^{6} x_{3},\\,x_{1}^{6} x_{2} x_{3},\\,x_{3}^{8},\\,x_{2}^{8},\\,x_{1} x_{3}^{7},\\,x_{2}^{7} x_{3},\\,x_{1} x_{2}^{7},\\,x_{1}^{6} x_{3}^{2},\\,x_{1}^{6} x_{2}^{2},\\,x_{1}^{7} x_{2},\\,x_{2}^{2} x_{3}^{6},\\,x_{2} x_{3}^{7},\\,x_{1}^{7} x_{3}\\right)</script></html>"
      ],
      "text/latex": [
       "\\begin{math}\n",
       "\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\left(x_{1}^{3} x_{2}^{3} x_{3}^{2},\\,x_{1}^{3} x_{2}^{2} x_{3}^{3},\\,x_{1}^{2} x_{2}^{3} x_{3}^{3},\\,x_{1}^{4} x_{2}^{2} x_{3}^{2},\\,x_{2}^{4} x_{3}^{4},\\,x_{1}^{4} x_{2}^{4},\\,x_{1}^{4} x_{3}^{4},\\,x_{1}^{4} x_{2}^{3} x_{3},\\,x_{1}^{3} x_{2}^{4} x_{3},\\,x_{1}^{3} x_{2} x_{3}^{4},\\,x_{1}^{4} x_{2} x_{3}^{3},\\,x_{1} x_{2}^{4} x_{3}^{3},\\,x_{1} x_{2}^{3} x_{3}^{4},\\,x_{1}^{2} x_{2}^{4} x_{3}^{2},\\,x_{1}^{5} x_{2}^{3},\\,x_{1}^{5} x_{3}^{3},\\,x_{2}^{5} x_{3}^{3},\\,x_{1}^{2} x_{2}^{2} x_{3}^{4},\\,x_{1}^{3} x_{2}^{5},\\,x_{1}^{3} x_{3}^{5},\\,x_{2}^{3} x_{3}^{5},\\,x_{1}^{2} x_{2}^{5} x_{3},\\,x_{1} x_{2}^{5} x_{3}^{2},\\,x_{1}^{5} x_{2}^{2} x_{3},\\,x_{1}^{2} x_{3}^{6},\\,x_{1}^{2} x_{2} x_{3}^{5},\\,x_{1} x_{2}^{2} x_{3}^{5},\\,x_{1}^{5} x_{2} x_{3}^{2},\\,x_{1}^{2} x_{2}^{6},\\,x_{2}^{6} x_{3}^{2},\\,x_{1}^{8},\\,x_{1} x_{2} x_{3}^{6},\\,x_{1} x_{2}^{6} x_{3},\\,x_{1}^{6} x_{2} x_{3},\\,x_{3}^{8},\\,x_{2}^{8},\\,x_{1} x_{3}^{7},\\,x_{2}^{7} x_{3},\\,x_{1} x_{2}^{7},\\,x_{1}^{6} x_{3}^{2},\\,x_{1}^{6} x_{2}^{2},\\,x_{1}^{7} x_{2},\\,x_{2}^{2} x_{3}^{6},\\,x_{2} x_{3}^{7},\\,x_{1}^{7} x_{3}\\right)\n",
       "\\end{math}"
      ],
      "text/plain": [
       "(x1^3*x2^3*x3^2, x1^3*x2^2*x3^3, x1^2*x2^3*x3^3, x1^4*x2^2*x3^2, x2^4*x3^4, x1^4*x2^4, x1^4*x3^4, x1^4*x2^3*x3, x1^3*x2^4*x3, x1^3*x2*x3^4, x1^4*x2*x3^3, x1*x2^4*x3^3, x1*x2^3*x3^4, x1^2*x2^4*x3^2, x1^5*x2^3, x1^5*x3^3, x2^5*x3^3, x1^2*x2^2*x3^4, x1^3*x2^5, x1^3*x3^5, x2^3*x3^5, x1^2*x2^5*x3, x1*x2^5*x3^2, x1^5*x2^2*x3, x1^2*x3^6, x1^2*x2*x3^5, x1*x2^2*x3^5, x1^5*x2*x3^2, x1^2*x2^6, x2^6*x3^2, x1^8, x1*x2*x3^6, x1*x2^6*x3, x1^6*x2*x3, x3^8, x2^8, x1*x3^7, x2^7*x3, x1*x2^7, x1^6*x3^2, x1^6*x2^2, x1^7*x2, x2^2*x3^6, x2*x3^7, x1^7*x3)"
      ]
     },
     "execution_count": 15,
     "metadata": {
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def create_monomials(n,d):\n",
    "    varz = [var('x%s'%i) for i in range(1,n+1)]\n",
    "    mons = []\n",
    "    for exps in IntegerVectors(d,n):\n",
    "        prod = varz[0] - varz[0] + 1\n",
    "        for i,x in enumerate(varz):\n",
    "            prod *= x^exps[i]\n",
    "        mons.append(prod)\n",
    "    return mons\n",
    "\n",
    "m = create_monomials(3,8)\n",
    "m_shuffled = P.T * vector(m)\n",
    "show(m_shuffled)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Below we sum the squares. We use each vector $\\ell$ to create a polynomial $\\ell m(x)$, which we square and sum with positive coefficient $d$. The resulting polynomial displayed below is therefore a sum of squares with positive coefficients."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\mathrm{True}</script></html>"
      ],
      "text/latex": [
       "\\begin{math}\n",
       "\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\mathrm{True}\n",
       "\\end{math}"
      ],
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 16,
     "metadata": {
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "okay = True\n",
    "for tup in terms:\n",
    "    d,l = tup[0],tup[1]\n",
    "    if d < 0:\n",
    "        okay = False\n",
    "show(okay)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{17}{9450} \\, x_{1}^{16} + \\frac{1}{1050} \\, x_{1}^{14} x_{2}^{2} + \\frac{1}{9450} \\, x_{1}^{12} x_{2}^{4} + \\frac{1}{525} \\, x_{1}^{10} x_{2}^{6} + \\frac{2}{315} \\, x_{1}^{8} x_{2}^{8} + \\frac{1}{525} \\, x_{1}^{6} x_{2}^{10} + \\frac{1}{9450} \\, x_{1}^{4} x_{2}^{12} + \\frac{1}{1050} \\, x_{1}^{2} x_{2}^{14} + \\frac{17}{9450} \\, x_{2}^{16} + \\frac{1}{1050} \\, x_{1}^{14} x_{3}^{2} - \\frac{16}{4725} \\, x_{1}^{12} x_{2}^{2} x_{3}^{2} - \\frac{8}{1575} \\, x_{1}^{10} x_{2}^{4} x_{3}^{2} + \\frac{11}{9450} \\, x_{1}^{8} x_{2}^{6} x_{3}^{2} + \\frac{11}{9450} \\, x_{1}^{6} x_{2}^{8} x_{3}^{2} - \\frac{8}{1575} \\, x_{1}^{4} x_{2}^{10} x_{3}^{2} - \\frac{16}{4725} \\, x_{1}^{2} x_{2}^{12} x_{3}^{2} + \\frac{1}{1050} \\, x_{2}^{14} x_{3}^{2} + \\frac{1}{9450} \\, x_{1}^{12} x_{3}^{4} - \\frac{8}{1575} \\, x_{1}^{10} x_{2}^{2} x_{3}^{4} - \\frac{11}{4725} \\, x_{1}^{8} x_{2}^{4} x_{3}^{4} - \\frac{1}{1890} \\, x_{1}^{6} x_{2}^{6} x_{3}^{4} - \\frac{11}{4725} \\, x_{1}^{4} x_{2}^{8} x_{3}^{4} - \\frac{8}{1575} \\, x_{1}^{2} x_{2}^{10} x_{3}^{4} + \\frac{1}{9450} \\, x_{2}^{12} x_{3}^{4} + \\frac{1}{525} \\, x_{1}^{10} x_{3}^{6} + \\frac{11}{9450} \\, x_{1}^{8} x_{2}^{2} x_{3}^{6} - \\frac{1}{1890} \\, x_{1}^{6} x_{2}^{4} x_{3}^{6} - \\frac{1}{1890} \\, x_{1}^{4} x_{2}^{6} x_{3}^{6} + \\frac{11}{9450} \\, x_{1}^{2} x_{2}^{8} x_{3}^{6} + \\frac{1}{525} \\, x_{2}^{10} x_{3}^{6} + \\frac{2}{315} \\, x_{1}^{8} x_{3}^{8} + \\frac{11}{9450} \\, x_{1}^{6} x_{2}^{2} x_{3}^{8} - \\frac{11}{4725} \\, x_{1}^{4} x_{2}^{4} x_{3}^{8} + \\frac{11}{9450} \\, x_{1}^{2} x_{2}^{6} x_{3}^{8} + \\frac{2}{315} \\, x_{2}^{8} x_{3}^{8} + \\frac{1}{525} \\, x_{1}^{6} x_{3}^{10} - \\frac{8}{1575} \\, x_{1}^{4} x_{2}^{2} x_{3}^{10} - \\frac{8}{1575} \\, x_{1}^{2} x_{2}^{4} x_{3}^{10} + \\frac{1}{525} \\, x_{2}^{6} x_{3}^{10} + \\frac{1}{9450} \\, x_{1}^{4} x_{3}^{12} - \\frac{16}{4725} \\, x_{1}^{2} x_{2}^{2} x_{3}^{12} + \\frac{1}{9450} \\, x_{2}^{4} x_{3}^{12} + \\frac{1}{1050} \\, x_{1}^{2} x_{3}^{14} + \\frac{1}{1050} \\, x_{2}^{2} x_{3}^{14} + \\frac{17}{9450} \\, x_{3}^{16}</script></html>"
      ],
      "text/latex": [
       "\\begin{math}\n",
       "\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{17}{9450} \\, x_{1}^{16} + \\frac{1}{1050} \\, x_{1}^{14} x_{2}^{2} + \\frac{1}{9450} \\, x_{1}^{12} x_{2}^{4} + \\frac{1}{525} \\, x_{1}^{10} x_{2}^{6} + \\frac{2}{315} \\, x_{1}^{8} x_{2}^{8} + \\frac{1}{525} \\, x_{1}^{6} x_{2}^{10} + \\frac{1}{9450} \\, x_{1}^{4} x_{2}^{12} + \\frac{1}{1050} \\, x_{1}^{2} x_{2}^{14} + \\frac{17}{9450} \\, x_{2}^{16} + \\frac{1}{1050} \\, x_{1}^{14} x_{3}^{2} - \\frac{16}{4725} \\, x_{1}^{12} x_{2}^{2} x_{3}^{2} - \\frac{8}{1575} \\, x_{1}^{10} x_{2}^{4} x_{3}^{2} + \\frac{11}{9450} \\, x_{1}^{8} x_{2}^{6} x_{3}^{2} + \\frac{11}{9450} \\, x_{1}^{6} x_{2}^{8} x_{3}^{2} - \\frac{8}{1575} \\, x_{1}^{4} x_{2}^{10} x_{3}^{2} - \\frac{16}{4725} \\, x_{1}^{2} x_{2}^{12} x_{3}^{2} + \\frac{1}{1050} \\, x_{2}^{14} x_{3}^{2} + \\frac{1}{9450} \\, x_{1}^{12} x_{3}^{4} - \\frac{8}{1575} \\, x_{1}^{10} x_{2}^{2} x_{3}^{4} - \\frac{11}{4725} \\, x_{1}^{8} x_{2}^{4} x_{3}^{4} - \\frac{1}{1890} \\, x_{1}^{6} x_{2}^{6} x_{3}^{4} - \\frac{11}{4725} \\, x_{1}^{4} x_{2}^{8} x_{3}^{4} - \\frac{8}{1575} \\, x_{1}^{2} x_{2}^{10} x_{3}^{4} + \\frac{1}{9450} \\, x_{2}^{12} x_{3}^{4} + \\frac{1}{525} \\, x_{1}^{10} x_{3}^{6} + \\frac{11}{9450} \\, x_{1}^{8} x_{2}^{2} x_{3}^{6} - \\frac{1}{1890} \\, x_{1}^{6} x_{2}^{4} x_{3}^{6} - \\frac{1}{1890} \\, x_{1}^{4} x_{2}^{6} x_{3}^{6} + \\frac{11}{9450} \\, x_{1}^{2} x_{2}^{8} x_{3}^{6} + \\frac{1}{525} \\, x_{2}^{10} x_{3}^{6} + \\frac{2}{315} \\, x_{1}^{8} x_{3}^{8} + \\frac{11}{9450} \\, x_{1}^{6} x_{2}^{2} x_{3}^{8} - \\frac{11}{4725} \\, x_{1}^{4} x_{2}^{4} x_{3}^{8} + \\frac{11}{9450} \\, x_{1}^{2} x_{2}^{6} x_{3}^{8} + \\frac{2}{315} \\, x_{2}^{8} x_{3}^{8} + \\frac{1}{525} \\, x_{1}^{6} x_{3}^{10} - \\frac{8}{1575} \\, x_{1}^{4} x_{2}^{2} x_{3}^{10} - \\frac{8}{1575} \\, x_{1}^{2} x_{2}^{4} x_{3}^{10} + \\frac{1}{525} \\, x_{2}^{6} x_{3}^{10} + \\frac{1}{9450} \\, x_{1}^{4} x_{3}^{12} - \\frac{16}{4725} \\, x_{1}^{2} x_{2}^{2} x_{3}^{12} + \\frac{1}{9450} \\, x_{2}^{4} x_{3}^{12} + \\frac{1}{1050} \\, x_{1}^{2} x_{3}^{14} + \\frac{1}{1050} \\, x_{2}^{2} x_{3}^{14} + \\frac{17}{9450} \\, x_{3}^{16}\n",
       "\\end{math}"
      ],
      "text/plain": [
       "17/9450*x1^16 + 1/1050*x1^14*x2^2 + 1/9450*x1^12*x2^4 + 1/525*x1^10*x2^6 + 2/315*x1^8*x2^8 + 1/525*x1^6*x2^10 + 1/9450*x1^4*x2^12 + 1/1050*x1^2*x2^14 + 17/9450*x2^16 + 1/1050*x1^14*x3^2 - 16/4725*x1^12*x2^2*x3^2 - 8/1575*x1^10*x2^4*x3^2 + 11/9450*x1^8*x2^6*x3^2 + 11/9450*x1^6*x2^8*x3^2 - 8/1575*x1^4*x2^10*x3^2 - 16/4725*x1^2*x2^12*x3^2 + 1/1050*x2^14*x3^2 + 1/9450*x1^12*x3^4 - 8/1575*x1^10*x2^2*x3^4 - 11/4725*x1^8*x2^4*x3^4 - 1/1890*x1^6*x2^6*x3^4 - 11/4725*x1^4*x2^8*x3^4 - 8/1575*x1^2*x2^10*x3^4 + 1/9450*x2^12*x3^4 + 1/525*x1^10*x3^6 + 11/9450*x1^8*x2^2*x3^6 - 1/1890*x1^6*x2^4*x3^6 - 1/1890*x1^4*x2^6*x3^6 + 11/9450*x1^2*x2^8*x3^6 + 1/525*x2^10*x3^6 + 2/315*x1^8*x3^8 + 11/9450*x1^6*x2^2*x3^8 - 11/4725*x1^4*x2^4*x3^8 + 11/9450*x1^2*x2^6*x3^8 + 2/315*x2^8*x3^8 + 1/525*x1^6*x3^10 - 8/1575*x1^4*x2^2*x3^10 - 8/1575*x1^2*x2^4*x3^10 + 1/525*x2^6*x3^10 + 1/9450*x1^4*x3^12 - 16/4725*x1^2*x2^2*x3^12 + 1/9450*x2^4*x3^12 + 1/1050*x1^2*x3^14 + 1/1050*x2^2*x3^14 + 17/9450*x3^16"
      ]
     },
     "execution_count": 17,
     "metadata": {
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ans = []\n",
    "for tup in terms:\n",
    "    d,l = tup[0],tup[1]\n",
    "    term = d * (l*m_shuffled)^2\n",
    "    term = term.expand()\n",
    "    ans.append(term)\n",
    "\n",
    "res = sum(ans)\n",
    "show(res.simplify())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Below we create the symmetric polynomial $H_{44} - H_{521}$, evaluated at $x_1^2, x_2^2, x_3^2$ to check nonnegativity on the nonnegative orthant, as explained in https://arxiv.org/abs/1909.00081. The nonnegativity of this polynomial provides the counterexample to the conjecture."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{17}{9450} \\, x_{1}^{16} + \\frac{1}{1050} \\, x_{1}^{14} x_{2}^{2} + \\frac{1}{9450} \\, x_{1}^{12} x_{2}^{4} + \\frac{1}{525} \\, x_{1}^{10} x_{2}^{6} + \\frac{2}{315} \\, x_{1}^{8} x_{2}^{8} + \\frac{1}{525} \\, x_{1}^{6} x_{2}^{10} + \\frac{1}{9450} \\, x_{1}^{4} x_{2}^{12} + \\frac{1}{1050} \\, x_{1}^{2} x_{2}^{14} + \\frac{17}{9450} \\, x_{2}^{16} + \\frac{1}{1050} \\, x_{1}^{14} x_{3}^{2} - \\frac{16}{4725} \\, x_{1}^{12} x_{2}^{2} x_{3}^{2} - \\frac{8}{1575} \\, x_{1}^{10} x_{2}^{4} x_{3}^{2} + \\frac{11}{9450} \\, x_{1}^{8} x_{2}^{6} x_{3}^{2} + \\frac{11}{9450} \\, x_{1}^{6} x_{2}^{8} x_{3}^{2} - \\frac{8}{1575} \\, x_{1}^{4} x_{2}^{10} x_{3}^{2} - \\frac{16}{4725} \\, x_{1}^{2} x_{2}^{12} x_{3}^{2} + \\frac{1}{1050} \\, x_{2}^{14} x_{3}^{2} + \\frac{1}{9450} \\, x_{1}^{12} x_{3}^{4} - \\frac{8}{1575} \\, x_{1}^{10} x_{2}^{2} x_{3}^{4} - \\frac{11}{4725} \\, x_{1}^{8} x_{2}^{4} x_{3}^{4} - \\frac{1}{1890} \\, x_{1}^{6} x_{2}^{6} x_{3}^{4} - \\frac{11}{4725} \\, x_{1}^{4} x_{2}^{8} x_{3}^{4} - \\frac{8}{1575} \\, x_{1}^{2} x_{2}^{10} x_{3}^{4} + \\frac{1}{9450} \\, x_{2}^{12} x_{3}^{4} + \\frac{1}{525} \\, x_{1}^{10} x_{3}^{6} + \\frac{11}{9450} \\, x_{1}^{8} x_{2}^{2} x_{3}^{6} - \\frac{1}{1890} \\, x_{1}^{6} x_{2}^{4} x_{3}^{6} - \\frac{1}{1890} \\, x_{1}^{4} x_{2}^{6} x_{3}^{6} + \\frac{11}{9450} \\, x_{1}^{2} x_{2}^{8} x_{3}^{6} + \\frac{1}{525} \\, x_{2}^{10} x_{3}^{6} + \\frac{2}{315} \\, x_{1}^{8} x_{3}^{8} + \\frac{11}{9450} \\, x_{1}^{6} x_{2}^{2} x_{3}^{8} - \\frac{11}{4725} \\, x_{1}^{4} x_{2}^{4} x_{3}^{8} + \\frac{11}{9450} \\, x_{1}^{2} x_{2}^{6} x_{3}^{8} + \\frac{2}{315} \\, x_{2}^{8} x_{3}^{8} + \\frac{1}{525} \\, x_{1}^{6} x_{3}^{10} - \\frac{8}{1575} \\, x_{1}^{4} x_{2}^{2} x_{3}^{10} - \\frac{8}{1575} \\, x_{1}^{2} x_{2}^{4} x_{3}^{10} + \\frac{1}{525} \\, x_{2}^{6} x_{3}^{10} + \\frac{1}{9450} \\, x_{1}^{4} x_{3}^{12} - \\frac{16}{4725} \\, x_{1}^{2} x_{2}^{2} x_{3}^{12} + \\frac{1}{9450} \\, x_{2}^{4} x_{3}^{12} + \\frac{1}{1050} \\, x_{1}^{2} x_{3}^{14} + \\frac{1}{1050} \\, x_{2}^{2} x_{3}^{14} + \\frac{17}{9450} \\, x_{3}^{16}</script></html>"
      ],
      "text/latex": [
       "\\begin{math}\n",
       "\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{17}{9450} \\, x_{1}^{16} + \\frac{1}{1050} \\, x_{1}^{14} x_{2}^{2} + \\frac{1}{9450} \\, x_{1}^{12} x_{2}^{4} + \\frac{1}{525} \\, x_{1}^{10} x_{2}^{6} + \\frac{2}{315} \\, x_{1}^{8} x_{2}^{8} + \\frac{1}{525} \\, x_{1}^{6} x_{2}^{10} + \\frac{1}{9450} \\, x_{1}^{4} x_{2}^{12} + \\frac{1}{1050} \\, x_{1}^{2} x_{2}^{14} + \\frac{17}{9450} \\, x_{2}^{16} + \\frac{1}{1050} \\, x_{1}^{14} x_{3}^{2} - \\frac{16}{4725} \\, x_{1}^{12} x_{2}^{2} x_{3}^{2} - \\frac{8}{1575} \\, x_{1}^{10} x_{2}^{4} x_{3}^{2} + \\frac{11}{9450} \\, x_{1}^{8} x_{2}^{6} x_{3}^{2} + \\frac{11}{9450} \\, x_{1}^{6} x_{2}^{8} x_{3}^{2} - \\frac{8}{1575} \\, x_{1}^{4} x_{2}^{10} x_{3}^{2} - \\frac{16}{4725} \\, x_{1}^{2} x_{2}^{12} x_{3}^{2} + \\frac{1}{1050} \\, x_{2}^{14} x_{3}^{2} + \\frac{1}{9450} \\, x_{1}^{12} x_{3}^{4} - \\frac{8}{1575} \\, x_{1}^{10} x_{2}^{2} x_{3}^{4} - \\frac{11}{4725} \\, x_{1}^{8} x_{2}^{4} x_{3}^{4} - \\frac{1}{1890} \\, x_{1}^{6} x_{2}^{6} x_{3}^{4} - \\frac{11}{4725} \\, x_{1}^{4} x_{2}^{8} x_{3}^{4} - \\frac{8}{1575} \\, x_{1}^{2} x_{2}^{10} x_{3}^{4} + \\frac{1}{9450} \\, x_{2}^{12} x_{3}^{4} + \\frac{1}{525} \\, x_{1}^{10} x_{3}^{6} + \\frac{11}{9450} \\, x_{1}^{8} x_{2}^{2} x_{3}^{6} - \\frac{1}{1890} \\, x_{1}^{6} x_{2}^{4} x_{3}^{6} - \\frac{1}{1890} \\, x_{1}^{4} x_{2}^{6} x_{3}^{6} + \\frac{11}{9450} \\, x_{1}^{2} x_{2}^{8} x_{3}^{6} + \\frac{1}{525} \\, x_{2}^{10} x_{3}^{6} + \\frac{2}{315} \\, x_{1}^{8} x_{3}^{8} + \\frac{11}{9450} \\, x_{1}^{6} x_{2}^{2} x_{3}^{8} - \\frac{11}{4725} \\, x_{1}^{4} x_{2}^{4} x_{3}^{8} + \\frac{11}{9450} \\, x_{1}^{2} x_{2}^{6} x_{3}^{8} + \\frac{2}{315} \\, x_{2}^{8} x_{3}^{8} + \\frac{1}{525} \\, x_{1}^{6} x_{3}^{10} - \\frac{8}{1575} \\, x_{1}^{4} x_{2}^{2} x_{3}^{10} - \\frac{8}{1575} \\, x_{1}^{2} x_{2}^{4} x_{3}^{10} + \\frac{1}{525} \\, x_{2}^{6} x_{3}^{10} + \\frac{1}{9450} \\, x_{1}^{4} x_{3}^{12} - \\frac{16}{4725} \\, x_{1}^{2} x_{2}^{2} x_{3}^{12} + \\frac{1}{9450} \\, x_{2}^{4} x_{3}^{12} + \\frac{1}{1050} \\, x_{1}^{2} x_{3}^{14} + \\frac{1}{1050} \\, x_{2}^{2} x_{3}^{14} + \\frac{17}{9450} \\, x_{3}^{16}\n",
       "\\end{math}"
      ],
      "text/plain": [
       "17/9450*x1^16 + 1/1050*x1^14*x2^2 + 1/9450*x1^12*x2^4 + 1/525*x1^10*x2^6 + 2/315*x1^8*x2^8 + 1/525*x1^6*x2^10 + 1/9450*x1^4*x2^12 + 1/1050*x1^2*x2^14 + 17/9450*x2^16 + 1/1050*x1^14*x3^2 - 16/4725*x1^12*x2^2*x3^2 - 8/1575*x1^10*x2^4*x3^2 + 11/9450*x1^8*x2^6*x3^2 + 11/9450*x1^6*x2^8*x3^2 - 8/1575*x1^4*x2^10*x3^2 - 16/4725*x1^2*x2^12*x3^2 + 1/1050*x2^14*x3^2 + 1/9450*x1^12*x3^4 - 8/1575*x1^10*x2^2*x3^4 - 11/4725*x1^8*x2^4*x3^4 - 1/1890*x1^6*x2^6*x3^4 - 11/4725*x1^4*x2^8*x3^4 - 8/1575*x1^2*x2^10*x3^4 + 1/9450*x2^12*x3^4 + 1/525*x1^10*x3^6 + 11/9450*x1^8*x2^2*x3^6 - 1/1890*x1^6*x2^4*x3^6 - 1/1890*x1^4*x2^6*x3^6 + 11/9450*x1^2*x2^8*x3^6 + 1/525*x2^10*x3^6 + 2/315*x1^8*x3^8 + 11/9450*x1^6*x2^2*x3^8 - 11/4725*x1^4*x2^4*x3^8 + 11/9450*x1^2*x2^6*x3^8 + 2/315*x2^8*x3^8 + 1/525*x1^6*x3^10 - 8/1575*x1^4*x2^2*x3^10 - 8/1575*x1^2*x2^4*x3^10 + 1/525*x2^6*x3^10 + 1/9450*x1^4*x3^12 - 16/4725*x1^2*x2^2*x3^12 + 1/9450*x2^4*x3^12 + 1/1050*x1^2*x3^14 + 1/1050*x2^2*x3^14 + 17/9450*x3^16"
      ]
     },
     "execution_count": 18,
     "metadata": {
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def input_squares(p):\n",
    "    # input is our polynomial p, like H_(2,1) - H_(1,1,1)\n",
    "    # output will be inserting each variable squared, also making it start at x1, x2,... rather than x0,x1,...\n",
    "    current_vars = p.args() # this is a tuple of variables x0, x1, x2,... etc.\n",
    "    numvars = len(current_vars)\n",
    "    new_vars = [var('x%i'%i) for i in range(1,numvars+1)]\n",
    "    subz = {}\n",
    "    for i,vr in enumerate(current_vars):\n",
    "        subz[vr] = (new_vars[i])^2\n",
    "    ans = p.subs(subz)\n",
    "    return ans\n",
    "\n",
    "def plugin_ones(p):\n",
    "    # want to return the number/scalar that results from plugging in 1's to all variables\n",
    "    current_vars = p.args() # this is a tuple of variables x0, x1, x2,... etc.\n",
    "    numvars = len(current_vars)\n",
    "    subz = {}\n",
    "    for i,vr in enumerate(current_vars):\n",
    "        subz[vr] = 1\n",
    "    ans = p.subs(subz)\n",
    "    return ans\n",
    "\n",
    "def term_normalize(p):\n",
    "    norm = plugin_ones(p)\n",
    "    ans = p/norm\n",
    "    return ans\n",
    "\n",
    "def create_difference(la,mu,n=3):\n",
    "    h = SymmetricFunctions(QQ).h()\n",
    "    # we will return the difference h_la - h_mu\n",
    "    hla = h(la).expand(n)\n",
    "    hmu = h(mu).expand(n)\n",
    "    hla = input_squares(hla)\n",
    "    hmu = input_squares(hmu)\n",
    "    hla = term_normalize(hla)\n",
    "    hmu = term_normalize(hmu)\n",
    "    ans = hla - hmu\n",
    "    return ans\n",
    "\n",
    "show(create_difference((4,4),(5,2,1)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Below we verify that our previous sum of squares reproduces the desired polynomial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 19,
     "metadata": {
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "res - create_difference((4,4),(5,2,1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
   ],
   "source": [
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "SageMath 9.2",
   "language": "sagemath",
   "metadata": {
    "cocalc": {
     "description": "Open-source mathematical software system",
     "priority": 10,
     "url": "https://www.sagemath.org/"
    }
   },
   "name": "sage-9.2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}