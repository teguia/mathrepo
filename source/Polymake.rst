####################
 Polymake
####################

-  :doc:`compday/index`

-  :doc:`InvitationToNonlinearAlgebra/index`

-  :doc:`OrdersPolytropes/index`

..
   -  :doc: `OrdersPolytropes/index.rst`
 
.. toctree::
   :maxdepth: 0
