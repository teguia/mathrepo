========================
The Gaussian Moduli
========================

| This page contains supplementary code to the following paper:
| Mateusz Michałek, Leonid Monin, and Jarosław Wiśniewski: Maximum likelihood degree, complete quadrics and C∗-action
| In: SIAM journal on applied algebra and geometry, 5 (2021) 1, p. 60-85
| DOI: `10.1137/20M1335960 <https://dx.doi.org/10.1137/20M1335960>`_ ARXIV: https://arxiv.org/abs/2004.07735 CODE: https://mathrepo.mis.mpg.de/GaussianModuli


The first file is in :math:`\verb|Macaulay2|`.

The second file is in :math:`\verb|Sage|`.

   :download:`LGM.txt <LGM.txt>`

   :download:`LGS.txt <LGS.txt>`
