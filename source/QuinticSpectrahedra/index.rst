=======================================
Quintic Spectrahedra
=======================================



| This page contains auxiliary files to the paper:
| Taylor Brysiewicz, Khazhgali Kozhasov, and Mario Kummer: Nodes on quintic spectrahedra
| In: Le Matematiche, 76 (2021) 2, p. 415-430
| DOI: `10.4418/2021.76.2.8 <https://dx.doi.org/10.4418/2021.76.2.8>`_ ARXIV: https://arxiv.org/abs/2011.13860 CODE: https://mathrepo.mis.mpg.de/QuinticSpectrahedra


In this article, we classify all generic combinatorial types of quintic spectrahedra. 
We consider generic linear spaces of real symmetric 5x5 matrices of the form 

.. math::
  A(x)=x_1Id+x_2A_2+x_3A_3+x_4A_4

and their corresponding quintic spectrahedra

.. math::
  \{x \in \mathbb{RP}^3 \mid A(x) \text{ is semidefinite}\}.

The algebraic boundary of such a spectrahedron is called a symmetroid. This is an algebraic surface of degree 5 in complex projective 3-space defined by the equation

.. math::
  \text{det}(A(x))=0.

Generic quintic symmetroids have 20 complex singularities. Some of those 20 singularities are real, and some of those real singularities lie on the Euclidean boundary of the spectrahedron.
The combinatorial type of a quintic spectrahedron is given by the pair

.. math::
  (\rho,\sigma) = (|\text{real singularities}|,|\text{real singularities on Euclidean boundary of spectrahedron}|)

In our article, we first show there are at most 65 combinatorial types of quintic spectrahedron (Corollary 2.10). 
Subsequently, we compute explicit witnesses of spectrahedra for each combinatorial type using numerical algebraic geometry.
Below is an image of all 65 types.

.. image:: 65Spectrahedra.png


The following Jupyter notebook, written in `julia <https://julialang.org/>`_ , verifies that our 65 examples are correct.
The computations rely heavily on the package `HomotopyContinuation.jl <https://www.juliahomotopycontinuation.org/>`_.


.. toctree::
	:maxdepth: 1
	:glob:

	QuinticSpectrahedraComputations



The certification files verifying the correctness of our solutions may be downloaded here :download:`Certificates.zip <Certificates.zip>`. 
For each of the 65 combinatorial types, there are two files. 

(1) Contains the certificates produced by HomotopyContinuation.jl for 64 solutions. 
Only 20 of these solutions have their first bounding interval containing zero: these are the 20 nodes of the corresponding symmetroid.
For each of these 20 solutions which are real, zero is not contained in the subsequent 25 intervals which encode the 1x1, 2x2, and 3x3 principal minors of the corresponding matrix.
Therefore, the signs of each of these intervals determine the signs of the principal minors, and thus whether the matrix is semidefinite. 


(2) Contains the polynomial system whose solutions are the nodes of a quintic symmetroid.
In particular, this system includes the affine chart used for certification since the success of certification depends on the chart chosen.


Visualizations of each type of quintic spectrahedra (as .jsurf and .png files) may be downloaded here :download:`SpectrahedraGallery.zip <SpectrahedraGallery.zip>`. 

The Jupyter notebook can be downloaded here: :download:`QuinticSpectrahedraComputations.ipynb`.

Project page created: 27/11/2020

Code  contributors: Taylor Brysiewicz

Jupyter Notebook written by: Taylor Brysiewicz, 27/11/2020

Corresponding author of this page: Taylor Brysiewicz, Taylor.Brysiewicz@mis.mpg.de


