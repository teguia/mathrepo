####################
 Magma
####################

-  :doc:`compday/index`

-  :doc:`tropicalProjections/index`

-  :doc:`Tsurfaces2Acurves/index`

-  :doc:`GaussianEntropyMap/index`

-  :doc:`27pAdicLines/index`

-  :doc:`octanomial/index`

-  :doc:`spaceSexticCurves/index`


.. toctree::
   :maxdepth: 0
