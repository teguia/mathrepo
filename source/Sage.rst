####################
 SageMath
####################


-  :doc:`ChipsplittingModels/index`

-  :doc:`CountingCubicHypersurfaces/index`

-  :doc:`correlated-equilibrium/index`

-  :doc:`MLdegreeCompleteQuadrics/index`

-  :doc:`compday/index`

-  :doc:`Tsurfaces2Acurves/index`

-  :doc:`FiberZonotopes/index`

-  :doc:`GaussianModuli/index`

-  :doc:`intersection-bodies/index`

-  :doc:`JordanAlgebrasSymmetricMatrices/index`

-  :doc:`logarithmicVoronoi/index`

-  :doc:`tensorminimality/index`

-  :doc:`TropicalInvariantsPicardCurves/index`

-  :doc:`SamplingpAdicManifolds/index`

-  :doc:`soscounterexample/index`

.. toctree::
   :maxdepth: 0
