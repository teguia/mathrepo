=========
Problem 7
=========

.. compday_problem7:

Draw 10000 samples from the set of positive definite symmetric :math:`5 \times 5`-matrices with trace one. Use
the distribution that Paul Breiding likes best. What is the expected value for the determinant of your
matrices?

Solution
========

Mathematica
-----------

Make a random matrix with determinant 1:

.. code-block:: mathematica

                RandomMatrix[n_] := Module[{upper, M},
                  upper = RandomReal[{0, 1}, {n, n}];
                  M = (upper + Transpose[upper])/(2 Tr[upper]);
                  Return[M]
                  ]

                RandomMatrix[3] // MatrixForm

A module that tests if the matrix is positive semidefinite, add the determinants of those, and waits until we
have done this M times:

.. code-block:: mathematica

                ExpectedDet[M_] := Module[{totalmatrices, totalsofar, Mat},
                  totalmatrices = 0;
                  totalsofar = 0;
                  While[totalsofar < M,
                   Mat = RandomMatrix[5];
                   If[
                    PositiveDefiniteMatrixQ[Mat],
                    totalsofar = totalsofar + 1;
                    totalmatrices = totalmatrices + Det[Mat];
                    ];
                   ];
                  Return[totalmatrices/totalsofar];
                  ]


The result. The first number is how long it took in seconds, the second is the expected value of the
determinant:

.. code-block:: mathematica

                Timing[ExpectedDet[10000]]
                
.. image:: 7.png
