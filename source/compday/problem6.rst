#########
Problem 6
#########

.. compday_problem6:

There is a package for Algebraic Statistics in *R*. Try this out, by analzying
the *Queen Victoria data set* seen in the Diaconis-Sturmfels paper.
