#########
Problem 4
#########

.. compday_problem4:
   
List all simplicial 3-spheres with 8 vertices, up to symmetry

Solution
--------

There are 39 such 3-spheres, as can be found `here`_.

.. _here: http://page.math.tu-berlin.de/~lutz/stellar/3-manifolds.html


