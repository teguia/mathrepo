==========
Problem 17
==========

.. compday_problem17:
   
The ``matlab`` command ``hilb(n)`` computes the Hilbert matrix of size :math:`n \times n`.
What are the eigenvalues and eigenvectors of this matrix, for various values of :math:`n`?
What can you say about the condition number?

Solution
========

Matlab
------

Computing the eigenvalues and eigenvectors of a matrix is performed via ``eig``:

.. code-block:: matlab

                [V,E] = eig( hilb(3) );

Show the eigenvalues:

.. code-block:: matlab

                diag(E)
                ans =

                  0.0026873
                  0.1223271
                  1.4083189
   
And the eigenvectors:

.. code-block:: matlab

                V
                V =

                  -0.12766   0.54745   0.82704
                  0.71375  -0.52829   0.45986
                  -0.68867  -0.64901   0.32330
   
The condition number of a matrix is computed with the command ``cond``:

.. code-block:: matlab

                for i=1:10
                  printf( "%d : %.6e\n", i, cond(hilb(i)) )
                end

Output:

.. code-block:: matlab

                1 : 1.000000e+00
                2 : 1.928147e+01
                3 : 5.240568e+02
                4 : 1.551374e+04
                5 : 4.766073e+05
                6 : 1.495106e+07
                7 : 4.753674e+08
                8 : 1.525758e+10
                9 : 4.931538e+11
                10 : 1.602442e+13
                
Mathematica
-----------

There is a built-in command called ``HilbertMatrix``:

.. code-block:: mathematica
                
                HilbertMatrix[3] // MatrixForm

Compute the eigenvalues and eigenvectors of the matrix (output is numerical):

.. code-block:: mathematica
                
                Eigensystem[HilbertMatrix[3]] // N
                Eigensystem[HilbertMatrix[4]] // N

How long does it take for :math:`n=10`:

.. code-block:: mathematica
                
                Timing[Eigensystem[HilbertMatrix[10]]][[1]]

A table of condition numbers, :math:`n` goes from 1 to 10:

.. code-block:: mathematica
                
                Table[LinearAlgebra`MatrixConditionNumber[HilbertMatrix[n]], {n, 1, 10}]

.. image:: 17.png
           


