=====================================================================
Third-Order Moment Varieties of Linear Non-Gaussian Graphical Models
=====================================================================

| Here may you find the supplementary codes for the paper: 
| Carlos Améndola, Mathias Drton, Alexandros Grosdos, Roser Homs and Elina Robeva: 
| Third-Order Moment Varieties of Linear Non-Gaussian Graphical Models
| ARXIV: https://arxiv.org/abs/2112.10875 
| CODE: https://mathrepo.mis.mpg.de/ThirdOrderMomentVarieties

*Abstract of the paper*: 
We study linear non-Gaussian graphical models from the perspective of algebraic statistics. 
These are acyclic causal models in which each variable is a linear combination of its direct causes and independent noise. 
The underlying directed causal graph can be identified uniquely via the set of second and third order moments of all random vectors that lie in the corresponding model. 
Our focus is on finding the algebraic relations among these moments for a given graph. We show that when the graph is a polytree these relations form a toric ideal. 
We construct explicit trek-matrices associated to 2-treks and 3-treks in the graph. Their entries are covariances and third order moments and their 2-minors define our model set-theoretically. 
Furthermore, we prove that their 2-minors also generate the vanishing ideal of the model. 
Finally, we describe the polytopes of third order moments and the ideals for models with hidden variables.
In this repository you will find the code used to compute all the examples in the paper.


.. toctree::
   :maxdepth: 2

   Examples.rst
   NonTrees.rst
 
If you want to run the code yourself, you may download the following *Macaulay2* file:
:download:`MomentsVarieties.m2 <MomentVarieties.m2>`  

The running example of the paper can be downloaded here:
:download:`Running example <Trees.m2>`   

The examples for non-trees can be downloaded here:
:download:`Non-trees <NonTrees.m2>`

Project page created: 28/01/2022.


Project contributors: Carlos Améndola, Mathias Drton, Alexandros Grosdos, Roser Homs and Elina Robeva.

Code written by: Carlos Améndola, Mathias Drton, Alexandros Grosdos, Roser Homs and Elina Robeva.

Corresponding author of this page: Roser Homs, roser.homs@tum.de.
