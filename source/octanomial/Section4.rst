=======================
Section 4: Dense Cubics
=======================

In Section 4 we look at cubic surfaces defined by a dense polynomial

.. math:: \begin{matrix}
  & c_0 w^3 + c_1w^2z + c_2wz^2 + c_3z^3 + c_4w^2y + c_5wyz + c_6yz^2 \\ & + \,
  c_7wy^2 + c_8y^2z  + c_9y^3  +  c_{10}w^2x  +
  c_{11}wxz + c_{12}xz^2 + c_{13}wxy \\ & + \,
  c_{14}xyz + c_{15}xy^2 + c_{16}wx^2 + c_{17}x^2z + c_{18}x^2y +
  c_{19}x^3.
  \end{matrix}

The magma code to transform such a surface into octanomial form and to study the tree arrangement of its line can be found in the github repository https://github.com/emresertoz/pAdicCubicSurface.

Computations are explicitly illustrated in Example 4.4 where we start from the following tropical coefficients vector

.. math:: ({\rm val}(c_0),\ldots,{\rm val}(c_{19})) \,\,=\,\,   (16,7,3,11,9,2,5,3,0,0,5,0,4,3,0,2,6,5,8,15).

We fix :math:`p=5` and choose the canonical lifts

.. math:: c_0 = 5^{16}, \,c_1 = 5^7\! ,\,c_2 = 5^3\!,\, \ldots, \,c_{19} = 5^{15}.


The six nearby points over :math:`\mathbb{Q}` of the blow-up points are 

.. math::
   \begin{array}{rcl}
   q_1 &=& ( 1 \,\,:\,\, 0\,\,:\,\, 0),\\
  q_2 &=& (0\,\,:\,\, 1 \,\,:\,\,0 ),\\
  q_3 &=& (0\,\,:\,\, 0\,\,:\,\,1 ),\\
  q_4 &=& (1\,\,:\,\, 1\,\,:\,\,1 ),\\
  q_5 &=& (2473616049/5 \,\,:\,\, -425393750 \,\,: \,\, 1 ), \\
  q_6 &=& (1331718750 \,\,:\,\, -2324221875 \,\,:\, \, 1 ). 
   \end{array}

We choose the point :math:`q_7=( -4: 10: -1) \in \mathbb{Q}` and proceed by finding a  cuspidal cubic over :math:`\mathbb{Q}_p` passing throught the :math:`7` points. Finally we compute the linear forms :math:`\ell_0, \ell_1, \ell_2` determining the automorphism of :math:`\mathbb{P}^2` which brings the cuspidal cubic in the standard form :math:`\{X^2Z=Y^3\}` and allows us to read the moduli paramaters :math:`d_1, d_2, \ldots, d_6`.

All the details and outputs of this computation can be found in the following :download:`magma file <successful_perturbation.mag>`.
