####################
 Mathematica
####################

-  :doc:`correlated-equilibrium/index`

-  :doc:`compday/index`

-  :doc:`farey/index`

-  :doc:`ConditionalIngleton/index`

-  :doc:`LyapunovIdentifiability/index`

-  :doc:`planeSexticCurves/index`

-  :doc:`spaceSexticCurves/index`

-  :doc:`StagedTreesWithToricStructures/index`


.. toctree::
   :maxdepth: 0
