push!(LOAD_PATH,pwd())

using MultivariatePolynomials
using Polymake

# used to make facet matrix of the form Ft = [I; ...]
function isnonIDRow(a)
    !( (count(ai->ai!=0,a)==1) & (count(ai->ai==1,a)==1) )
end

## Polytopes

function facetRepresentation(P)
    # Computes the integer facet representation Fᵀm + a ≥ 0 of a lattice polytope P
    aFt = P.FACETS
    Ft = convert(Array{Rational{Int64}},aFt[:,2:end])
    a = convert(Array{Rational{Int64}},aFt[:,1])
    (k,n)=size(Ft)
    D = denominator.(Ft)
    k = size(Ft,1)
    for i = 1:k
        d = lcm(D[i,:])
        Ft[i,:] = d*Ft[i,:];
        a[i] = d*a[i];
    end
    Ft = convert(Array{Int64}, Ft)
    a = convert(Array{Int64}, a)
    return Ft,a
end

function facetRepresentation_id(P)
    # Computes the integer facet representation Fᵀm + a ≥ 0 of a lattice polytope P
    aFt = P.FACETS
    Ft = convert(Array{Rational{Int64}},aFt[:,2:end])
    a = convert(Array{Rational{Int64}},aFt[:,1])
    ## fix output of Polymake so Ft=[ID;...]
    (k,n)=size(Ft)
    ID = Matrix(1*I,n,n)
    nonIDIndices = filter(i-> isnonIDRow(Ft[i,:]), 1:k)
    Ft = [ID; Ft[nonIDIndices,:]]
    D = denominator.(Ft)
    k = size(Ft,1)
    for i = 1:k
        d = lcm(D[i,:])
        Ft[i,:] = d*Ft[i,:];
        a[i] = d*a[i];
    end
    Ft = convert(Array{Int64}, Ft)
    a = convert(Array{Int64}, a)
    return Ft,a
end


function vertexRepresentation(P)
    # Computes a matrix whose rows are the vertices of a lattice polytope P
    vtxmtx = P.VERTICES
    vtxmtx = convert(Array{Int64}, vtxmtx)
    vtxmtx = vtxmtx[:,2:end]
    return vtxmtx
end

function newtonPolytope(f)
    # computes the Newton polytope of the polynomial f
    trms = terms(f)
    n = length(variables(f))
    latPts = zeros(Int64, length(trms), n+1)
    for i = 1:length(trms)
        latPts[i,:] = [1 transpose(exponents(trms[i]))]
    end
    P = @pm polytope.Polytope(POINTS = latPts)
    return P
end

function convexHull(pts)
    ptsLifted = [ones(eltype(pts), 1, size(pts)[2]); pts]
    polytope.Polytope(POINTS = ptsLifted')
end

function newtonPolytope(f)
    pts = (support_coefficients(System([f])))[1][1]
    convexHull(pts)
end

# minkowski sum of an array
function sumAll(NPS)
    P = NPS[1]
    for i=2:length(NPS)
        P = polytope.minkowski_sum(P,NPS[i])
    end
    P
end
function getLatticePoints(P,x)
    # computes the lattice points in (a translated version of) P
    pts_α = P.LATTICE_POINTS_GENERATORS
    #println(pts_α)
    exps_α = convert(Array{Int64},pts_α[1][:,2:end])
    shift = abs.(minimum(vcat(exps_α,zeros(Int64,1,size(exps_α,2))),dims=1))
    #println(shift)
    exps_α = exps_α .+ shift
    return exps_α
end
