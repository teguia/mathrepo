push!(LOAD_PATH,pwd())

using MultivariatePolynomials
using Polymake
using SmithNormalForm  # to add: pkg> add https://github.com/wildart/SmithNormalForm.jl.git
using LinearAlgebra

include("Polytopes.jl")

function computePolytope(eq)
    SC = support_coefficients(System(eq));
    supp = SC[1];
    suppUnique=unique(supp)
    n = length(suppUnique);
    NPS = [convexHull(suppUnique[j]) for j=1:n];
    P=sumAll(NPS)
    Fᵀ,a = facetRepresentation(P)
    return P, Fᵀ, a
end

function baseEquations(P,Fᵀ,a,x)
    # computes generatorts of the irrelevant ideal (i.e. equations for the base locus) from a lattice polytope P
    # Fᵀm + a ≥ 0 should be a facet representation of P, e.g. via Fᵀ, a = facetRepresentation(P)
    # the variable x[i] corresponds to the ith row of Fᵀ
    vtxmtx = vertexRepresentation(P)
    E = Fᵀ*vtxmtx' .+ a
    dummy = sum(x) + 1.0;
    B = fill(dummy,size(E,2))
    for i = 1:size(E,2)
        B[i] = prod(x[findall(y->y!=0, E[:,i])])
    end
    return B
end

function orbitDegree(Fᵀ)
    # This computes the degree of a generic orbit the GIT quotient for any toric variety whose normal fan has rays equal to the rows of Fᵀ
    NF = smith(Fᵀ);
    P = round.(Int,inv(NF.S));
    (k,n) = size(Fᵀ);
    P̂ = hcat(zeros(Int,k,1),P);
    latPts = hcat(round.(Int,ones(k+1,1)), transpose(P̂[n+1:k,:]));
    Pol = @pm polytope.Polytope(POINTS = latPts);
    v = big(Pol.VOLUME)*factorial(k-n);
    deg = v*abs(prod(NF.SNF)); #degree is normalized volume times prod of invariant factors.
    return deg, P[n+1:k,:]
end

function homogenize(f̂, Fᵀ, x)
    # This function homogenizes the system f̂ and computes the divisor classes α of the equations in f
    k,n = size(Fᵀ)
    homogeneousDummy = (0.0+0.0im)*prod(x) + 0.0;
    α = fill(zeros(Int64,k,1),n)
    f = fill(zero(typeof(homogeneousDummy)),n)
    for j = 1:n
        termsf = terms(f̂[j])
        exponentMatrix = zeros(Int64,length(termsf),n+1)
        vrsj = variables(f̂[j])
        exponentMatrixj = zeros(Int64,length(termsf),length(vrsj))
        ptr = zeros(Int64,length(vrsj))''
        for i = 1:length(vrsj)
            ptr[i] = 1+ findfirst(isequal(vrsj[i]), variables(f̂))
        end
        for i = 1:length(termsf)
            exponentMatrix[i,hcat([1],ptr')] = [1 transpose(exponents(termsf[i]))];
        end
        α[j] = -minimum(Fᵀ*transpose(exponentMatrix[:,2:end]), dims = 2)
        homogeneousExponentMatrix = (Fᵀ*transpose(exponentMatrix[:,2:end]) + α[j]*ones(1,length(termsf)))
        homogeneousExponentMatrix = convert(Array{Int64},homogeneousExponentMatrix)
        homogeneousMonomials = map( i -> prod(x.^homogeneousExponentMatrix[:,i]), [1:size(homogeneousExponentMatrix,2);]);
        coefff = coefficients(f̂[j]);
        h = transpose(coefff)*homogeneousMonomials;
        f[j] = h;
    end
    return f, α
end

function dehomogenizeSolutions(homogeneousSols, Fᵀ)
    # This applies the quotient map to the points in homogeneousSols to obtain the corresponding points in the torus
    k,n = size(Fᵀ)
    randpoint = randn(ComplexF64,n)
    toricSols = fill(randpoint,length(homogeneousSols))
    for i = 1:length(homogeneousSols)
        toricSols[i] = map(j -> prod((homogeneousSols[i].+eps()).^Fᵀ[:,j]), [1:n;]);
    end
    return toricSols
end

function computeOrbitParametrization(Fᵀ, λ)
#computes the matrix A whose columns give the parametrization of the orbit's torus
#this also needs to return the torsion information, but that hasn't been implemented yet
    NF = smith(Fᵀ);
    P = round.(Int,inv(NF.S));
    (k,n) = size(Fᵀ);
    P2 = P[n+1:k,:];
    A = hcat(copy(P2),zeros(Int,k-n,1));
    #println(A)
    for i=1:size(A,1) #loop only necessary if Laurent polynomials are prohibited
        minRow = minimum(A[i,:]);
        if minRow <0
            A[i,:] = A[i,:] - fill(minRow, size(A,2));
        end
    end
    #println(A)
    param = map(k->prod(λ.^A[:,k]),1:size(A,2))
    return A, param
end
