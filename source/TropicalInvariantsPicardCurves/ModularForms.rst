==============================================================
Computing the modular forms :math:`\nu(I_4), \nu(I_8), \nu(I_{12}), \nu(I_{18})` and :math:`\nu(H)`.
==============================================================

This page contains the code to compute the modular forms arizing from the invariants :math:`I_4,I_8,I_{12},I_{18}` and :math:`H`.



.. code-block:: none

	K<a>:=NumberField(x^2+3);

	rho:=-1/2+a/2;

	R<c,g>:=FunctionField(K,2);

	R2<phi0,phi1,phi2,zeta,w>:=PolynomialRing(R,5);

	h:=zeta^3+rho/(3^7*a)*phi0*phi1*phi2*(phi1-phi0)*(phi2-phi0)*(phi2-phi1);

	R3<z0,z1,z2,z3,w>:=quo<R2|[h,w*zeta-1]>;



	E6 := phi0^2 + phi1^2 + phi2^2 - (2/3)*(phi0*phi1 + phi0*phi2 + phi1*phi2);  

	E9 := (-phi0 + phi1 + phi2)*(phi0 - phi1 + phi2 )*( phi0 + phi1 - phi2);

	E12 := (-1/3)*(phi0 + phi1 + phi2)*(-3*phi0 + phi1 + phi2 )*( phi0 - 3*phi1 + phi2)*(phi0 + phi1 - 3*phi2);


	E6 := Evaluate(E6,[z0,z1,z2,z3,w]);
	E9 := Evaluate(E9,[z0,z1,z2,z3,w]);
	E12 := Evaluate(E12,[z0,z1,z2,z3,w]);


	Chi12 := (E6^2 - E12)/5184;

	Chi18 := (E6^3 - E9^2)/3888;


	j2 := (c^4/(6*g^4)) * Chi12 * w^2;

	j3 := (c^6/(864*g^6))*(Chi18 - E6 * Chi18) * w^3;

	j5 := (3*c^4 / 70)*z3;

	j6 := (-c^6/(5040*g^2))*E6;

	j9 := (c^9 / (798336*g^3)) * E9;




	nu_I4 := ( 2*j2*j6  - j3*j5 ) / 3 ;

	nu_I8 := (14/9)*j2^2*j6^2 + (22/27)*j2^3*j5^2 + (5/27)*j3^2*j5^2 - (14/9)*j2*j3*j5*j6;

	nu_I12 := (4400/243)*j2^3*j6^3  - (11/243)*j3^2*j6^3 - (242/9)*j2^2*j3*j5*j6^2 + (2479/81)*j2^4*j5^2*j6 + (692/81)*j2*j3^2*j5^2*j6 - (7156/243)*j2^3*j3*j5^3 - (92/243)*j3^3*j5^3  ;

	nu_I18 := (-625/729)*j2^6*j5^3*j9  + (-512/729)*j2^3*j3^2*j5^3*j9 + (-4/729)*j2^3*j3*j6^3*j9 + (1/729)*j3^3*j6^3*j9 + (1/3)*j2^4*j3*j5^2*j6*j9 +  (4/243)*j2^5*j5*j6^2*j9 + (-1/243)*j2^2*j3^2*j5*j6^2*j9 ;


	alpha := 1/(250822656000);
	beta := 1/(299067455175152371993371049908040738485043200000000000000);
	    
	nu_H := beta * I12 -  396 * (alpha*I4)^3;




	// The invariants I4, I8, I12, I18 and H are now taken under the map nu.
	// We can see from these computations that these modular forms are not constant and are meromorphic modular forms in with order deg(I)/2 in zeta.



:download:`Modular_forms.m<Modular_forms.m>`


