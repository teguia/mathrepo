--code for computing the generators of the ideal of the tangential variety of 
--the Segre variety of P^1x...xP1 



--this function weightVar(i,z_{...}) returns l(i,j) if in the list {...} there is a j in position i
--i.e. it returns the weight of the variable with respect the i-th factor of the Segre
--INPUT: (integer i,variable in the coordinate ring of the Segre)
--OUTPUT: the corresponding weight
weightVar =  (i,t) -> (  
   t = last baseName t;
   RP := 0;
   for j from 0 to 1 do (
     if t_i == j then RP = RP + l_(i,j);
     );   
   return RP;
);

--this function generalizes weightVar to degree 2 monomials just adding the respective l_(i,j)'s
--INPUT: (factor i, monomial t of degree 2)
--OUTPUT: the corresponding weight
--functions used: weightVar
STquad = (i,t) -> (
    SP := 0;
    g := toList factor t/value;
    if length(g) == 1 then (
    g = factor t;
        SP = SP + 2*weightVar(i,g#0#0);----quadriche
    )    
    else SP = SP + weightVar(i,g_0) + weightVar(i,g_1);
    return SP;
); 

--this function generalizes weightVar to degree 3 monomials just adding the respective l_(i,j)'s
--INPUT: (factor i, monomial t of degree 3)
--OUTPUT: the corresponding weight
--functions used: weightVar
STcub = (i,t) -> (
    SP := 0;
    g := toList factor t/value;
    if length(g) == 1 then (
    g = factor t;
        SP = SP + 3*weightVar(i,g#0#0);
    )    
    else if length(g) == 2 then (
      g1 := factor g_0;
      g2 := factor g_1;
      if g1#0#1 == 1 then SP = SP + weightVar(i,g1#0#0) + 2*weightVar(i,g2#0#0)
      else SP = SP + 2*weightVar(i,g1#0#0) + weightVar(i,g2#0#0)
    )
    else SP = SP + weightVar(i,g_0) + weightVar(i,g_1) + weightVar(i,g_2);
    return SP;
); 

--this function generalizes weightVar to degree 4 monomials just adding the respective l_(i,j)'s
--INPUT: (factor i, monomial t of degree 4)
--OUTPUT: the corresponding weight
--functions used: weightVar
STquart = (i,t) -> (
    SP := 0;
    g := toList factor t/value;
    if length(g) == 1 then (
    g = factor t;
        SP = SP + 4*weightVar(i,g#0#0);
    )    
    else if length(g) == 2 then (
      g0 := factor g_0;
      g1 := factor g_1;
      if g0#0#1 == 1 then SP = SP + weightVar(i,g0#0#0) + 3*weightVar(i,g1#0#0)
      else if g0#0#1 == 3 then SP = SP + 3*weightVar(i,g0#0#0) + weightVar(i,g1#0#0)
      else if g0#0#1 == 2 then SP = SP + 2*weightVar(i,g0#0#0) + 2*weightVar(i,g1#0#0)
    )
    else if length(g) == 3 then (
      g0 = factor g_0;
      g1 = factor g_1;
      g2 := factor g_2;
      if g0#0#1 == 1 then (
      if g1#0#1 == 1 then SP = SP + weightVar(i,g0#0#0) + weightVar(i,g1#0#0) + 2*weightVar(i,g2#0#0)
      else SP = SP + weightVar(i,g0#0#0) + 2*weightVar(i,g1#0#0) + weightVar(i,g2#0#0)
      )
      else SP = SP + 2*weightVar(i,g0#0#0) + weightVar(i,g1#0#0) + weightVar(i,g2#0#0)
      )
    else SP = SP + weightVar(i,g_0) + weightVar(i,g_1) + weightVar(i,g_2) + weightVar(i,g_3);
    return SP;
); 

--this function generalizes weightVar to degree d monomials, for d=2,3,4 just adding the respective l_(i,j)'s
--INPUT: (factor i, monomial t of degree d)
--OUTPUT: the corresponding weight
--functions used: STquad, STcub, STquart
genWeightVar=(i,t)->(
    if degree t=={2} then(
        return STquad(i,t);
    )else if degree t=={3} then(
        return STcub(i,t);
    )else return STquart(i,t);
);

--this function creates the rings used to compute the equations of the tangential variety
--INPUT: number of factors n
--OUTPUT: coordinate ring of the corresponding Segre, corresponding ring of the weights
infoTangential=(n)->(
    A := {};
    C := {};
    i = 0; while i < n list A = append(A,0) do i = i+1;
    i = 0; while i < n list C = append(C,1) do i = i+1; 
    R := QQ[z_A..z_C]; 
    W := QQ[l_(0,0)..l_(n-1,0),l_(0,1)..l_(n-1,1)]; --these are weights 
    return(R,W);
    )

--INPUT: number of factors n and a list LM of cardinality either 3 or 4 whose integers represent either the 4 factors in which /\^2 appears or
--       the 4 factors in which the module S_(2,1) appears or the 3 factors in which the module S_(2,2) appears
--OUTPUT: a list ML where
--        in the case of quadrics, ML will be the list whose integers represent the factors in which Sym^2 appears.
--        in the case of cubics, ML will be the list whose integers represent the factors in which Sym^3 appears.
--        in the case of quartics, ML will be the list whose integers represent the factors in which Sym^4 appears.
listML=(n,LM)->(
    ML := {};
    for i from 0 to n-1 do ML = append(ML,i);
    for i from 0 to length(LM)-1 do ML = delete(LM_i,ML);
    return ML;
    )

--INPUT: (degree d, number of factors n)
--OUTPUT:a list Y where,
--       if d=2, then Y will contain all degree 2 mono whose weights w.r.t. the factors chosen by LM are the highest weight of /\^2
--       if d=3, then Y will contain all degree 3 mono whose weights w.r.t. the factors chosen by LM are the highest weight of S_(2,1)
--       if d=4, then Y will contain all degree 3 mono whose weights w.r.t. the factors chosen by LM are the highest weight of S_(2,1)
--functions used: infoTangential, STquad, STcub, STquart
listOfMon=(d,n,LM)->(
    (R,W):=infoTangential(n);
    Y:={};
    if d==2 then(
        B := flatten entries basis(2,R);
        for i from 0 to length(B)-1 do (
            k := 0;
            for j from 0 to 3 do (
                if STquad(LM_j,B_i) == l_(LM_j,0)+l_(LM_j,1) then k = k+1;
            );
            if k == 4 then Y = append(Y,B_i);
        ); 
        return Y;
    )else if d==3 then(
        B = flatten entries basis(3,R);    
        for i from 0 to length(B)-1 do (
            k = 0;
            for j from 0 to 3 do (
                if STcub(LM_j,B_i) == 2*l_(LM_j,0)+l_(LM_j,1) then k = k+1;
            );
            if k == 4 then Y = append(Y,B_i);
        ); 
        return Y;
    )else(
        B = flatten entries basis(4,R);
        for i from 0 to length(B)-1 do (
            k = 0;
            for j from 0 to 2 do (
                if STquart(LM_j,B_i) == 2*l_(LM_j,0)+2*l_(LM_j,1) then k = k+1;
            );
        if k == 3 then Y = append(Y,B_i);
        ); 
    );
    return Y;
 )

--for d=2 we remove from Y the el. that are not in /\^2, for d=3 we remove the el. not in Sym^3 and for d=4 we remove the el not in Sym^4
--INPUT: (list ML, degree d, list Y)
--OUTPUT: the new list Y with the removed elements 
--functions used: STquad, STcub, STquart
listOfDeleteMon=(ML,d,Y)->(
    YY:={};
    if d==2 then(
        for i from 0 to length(Y)-1 do (
            var=0;
                for j from 0 to length(ML)-1 do (
                    if STquad(ML_j,Y_i) != 2*l_(ML_j,0) then(
                        var = var+1;
                    );
                );
            if var >=1 then YY=append(YY,i);
        );
    )else if d==3 then(
        for i from 0 to length(Y)-1 do (
            var = 0;
            for j from 0 to length(ML)-1 do (
                if STcub(ML_j,Y_i) != 3*l_(ML_j,0) then var = var +1;
            );
            if var >= 1 then YY = append(YY,i);
        );
    )else(
        for i from 0 to length(Y)-1 do (
            var = 0;
            for j from 0 to length(ML)-1 do (
                if STquart(ML_j,Y_i) != 4*l_(ML_j,0) then var = var +1;
            );
            if var >= 1 then YY = append(YY,i);
        ); 
    );
    i = 0; while i < length(YY) list Y = drop(Y, {YY_i-i,YY_i-i}) do i = i+1;
    return Y;
)---SKIP THIS IF n=4

--this function represents the Lie Algebra action on a single variable
--INPUT:(index i,index j, factor k -1, variable), where the indices i,j are related to the matrix E_{i,j}
--OUTPUT: 
E = (i,j,k,t) -> (
    ti = last baseName t;
    if ti_k == j then (
      ti = drop(ti, {k,k});
      ti = insert(k,i,ti);
      return sub(z_ti,ring t);
      ) else return 0;
    )----R ring segre va richiamato


e = (i,j,k, Ri ) -> (
    BB:=flatten entries  basis(1,Ri);
    P := {};
    for l from 0 to length(BB)-1 do (
    P = append(P,E(i,j,k,BB_l));
    );
    return P;
    )
--this function returns a list useful to build the map y_(0,1,l) below



Fdeg2 = (i,j,l,t) -> (
ti := diff(basis(1,ring t),t);
ti = y_(i,j,l)(ti);
ti = ti*(transpose basis(1,ring t));
return ti;
) 
--- given a deg 2 monomial, it computes the derivative and then applies y_
--this is the Lie Algebra action on degree 2 hoogeneous elements 


--- similar to F for cubics
LAcub = (l,t) -> (
    f := 0;
    g := toList factor t/value;
    if length(g) == 1 then (
    op := factor t; 
        f = f + 3*y_(0,1,l)(op#0#0)*(op#0#0)^2;
    )   
    else if length(g) == 2 then (
    g0 := factor g_0;
    g1 := factor g_1;
    if g0#0#1 ==2 then 
          f = f + 2*y_(0,1,l)(g0#0#0)*(g0#0#0)*(g1#0#0) + y_(0,1,l)(g1#0#0)*(g0#0#0)^2
    else f = f + 2*y_(0,1,l)(g1#0#0)*(g1#0#0)*(g0#0#0) + y_(0,1,l)(g0#0#0)*(g1#0#0)^2
    )
    else f = f + y_(0,1,l)(g_0)*(g_1)*(g_2) +  y_(0,1,l)(g_1)*(g_0)*(g_2) +  y_(0,1,l)(g_2)*(g_1)*(g_0);
    return f;
    )
  
LAquart = (l,t) -> (
    f := 0;
    g := toList factor t/value;
    if length(g) == 1 then (
    op := factor t; 
        f = f + 4*y_(0,1,l)(op#0#0)*(op#0#0)^3;
    )   
    else if length(g) == 2 then (
    g0 = factor g_0;
    g1 = factor g_1;
    if g0#0#1 == 2 then (
          f = f + 2*y_(0,1,l)(g0#0#0)*(g0#0#0)*(g1#0#0)^2 + 2*y_(0,1,l)(g1#0#0)*(g1#0#0)*(g0#0#0)^2
      )
    else if g0#0#1 == 3 then (
        f = f + 3*y_(0,1,l)(g0#0#0)*(g1#0#0)*(g0#0#0)^2 + y_(0,1,l)(g1#0#0)*(g0#0#0)^3
        )
    else if g0#0#1 == 1 then (
        f = f + 3*y_(0,1,l)(g1#0#0)*(g0#0#0)*(g1#0#0)^2 + y_(0,1,l)(g0#0#0)*(g1#0#0)^3
        )
    )
    else if length(g) == 3 then (
        g0 = factor g_0;
        g1 = factor g_1;
        g2 := factor g_2;
    if g0#0#1 == 2 then (
        f = f + 2*y_(0,1,l)(g0#0#0)*(g0#0#0)*(g1#0#0)*(g2#0#0) + y_(0,1,l)(g1#0#0)*(g0#0#0)*(g0#0#0)*(g2#0#0) +  y_(0,1,l)(g2#0#0)*(g0#0#0)*(g0#0#0)*(g1#0#0) 
        )
    else if g0#0#1 == 1 then (
        if g1#0#1 == 1 then (
         f = f + 2*y_(0,1,l)(g2#0#0)*(g2#0#0)*(g1#0#0)*(g0#0#0) + y_(0,1,l)(g1#0#0)*(g0#0#0)*(g2#0#0)*(g2#0#0) +  y_(0,1,l)(g0#0#0)*(g2#0#0)*(g2#0#0)*(g1#0#0) 
        )
        else  f = f + 2*y_(0,1,l)(g1#0#0)*(g0#0#0)*(g1#0#0)*(g2#0#0) + y_(0,1,l)(g0#0#0)*(g1#0#0)*(g1#0#0)*(g2#0#0) +  y_(0,1,l)(g2#0#0)*(g1#0#0)*(g0#0#0)*(g1#0#0) 
        )
    )
    else f = f + y_(0,1,l)(g_0)*(g_1)*(g_2)*(g_3) +  y_(0,1,l)(g_1)*(g_0)*(g_2)*(g_3) +  y_(0,1,l)(g_2)*(g_1)*(g_0)*(g_3) + y_(0,1,l)(g_3)*(g_1)*(g_2)*(g_0);
    return f;
    )
    
F = (l,t,d) -> (
    GG := {};
    (M,C) := coefficients t;
    M = flatten entries M;
    C = flatten entries C;
    for i from 0 to length(M)-1 do( 
        if d==3 then(
            GG = append(GG,(C_i)*LAcub(l,M_i));
        )else(
            GG = append(GG,(C_i)*LAquart(l,M_i));
        );
    );    
    GG = sum(GG);
    return GG;
    )
--this is the Lie Algebra action on homogeneous pol. of degree 3
     
fromListToHwvq=(d,lm,n,R)->(
        ML := listML(n,lm);
        Y := listOfMon(d,n,lm);
        Y = listOfDeleteMon(ML,d,Y);
        S := QQ[a_0..a_(length Y-1)];
        Ri := S[flatten entries vars R];
        --use Ri;
        ff := n -> sum for i from 0 to n list a_i*sub(Y_i,Ri);
        hwvq := ff (length Y-1); 
        return (Ri,sub(hwvq,Ri),Y,ML);
        )

--in this function we apply all the F(0,1,i,-) to the general linear combination. If hwvq is the highest weight
--vector the results must all be zero. For now we will get only relations between the a_i's
--INPUT: the general linear combination of the monomials hwvq
--OUTPUT: list of relations between the coefficients of the hwvq
--functions used: F(i,j,k,t), F(l,t)
listOfrel=(d,hwvq,n)->(
    Tt := {};
    if d==2 then(
        for i from 0 to n-1 do
            Tt = append(Tt,Fdeg2(0,1,i,hwvq));
    )else(
         for i from 0 to n-1 do
            Tt = append(Tt,F(i,hwvq,d));
    );
    rel:= {};
    for i from 0 to length(Tt)-1 do (
        (M,C) = coefficients Tt_i;
        C = flatten entries C;
        rel = rel | C;
    );
    return rel;
)

deg2factors=(I,hwvq,Y,ML)->(
    Jac := jacobian I; --- diff(transpose basis(1,S),matrix mingens I);
    KK := mingens kernel transpose Jac;
    Q0 := {};
    for i from 0 to length Y-1 do(
        Q0 = append(Q0,a_i => (KK_0)_i);
    );    
    hwv := sub(hwvq,Q0);
    J := ideal();
    for i from 0 to length(ML)-1 do (
        J = J + ideal(hwv,Fdeg2(1,0,ML_i,hwv),Fdeg2(1,0,ML_i,Fdeg2(1,0,ML_i,hwv)))
    );
    return flatten entries mingens J;
)

degree2eqn=(R,W,d,n)->(
    LM := subsets(toList(0..n-1),4);
    quad := {};
    for lm in LM do(
        (Ri,hwvq,Y,ML) := fromListToHwvq(d,lm,n,R);
        for l from 0 to n-1 do(--usate anche per cub e quart
            y_(0,1,l) = map(Ri,Ri,e(0,1,l,Ri));---l fattore cub quart
            y_(1,0,l) = map(Ri,Ri,e(1,0,l,Ri));--quad
        );
        rel := listOfrel(d,hwvq,n);  
        I := ideal(rel);
        S := coefficientRing Ri;
        I = sub(ideal(mingens I),S);
        quad = append(quad,deg2factors(I,hwvq,Y,ML));
        );
    return quad;
    )

GAcub = (l,t) -> (
    f := 0;
    g := toList factor t/value;
    if length(g) == 1 then (
        op = factor t; 
        f = f + 3*r_(1,0,l)(op#0#0)*(op#0#0);
    )else if length(g) == 2 then (
        g0 = factor g_0;
        g1 = factor g_1;
        if g0#0#1 ==2 then 
            f = f + 2*r_(1,0,l)(g0#0#0)*(g0#0#0)*(g1#0#0) + r_(1,0,l)(g1#0#0)*(g0#0#0)^2
        else f = f + 2*r_(1,0,l)(g1#0#0)*(g1#0#0)*(g0#0#0) + r_(1,0,l)(g0#0#0)*(g1#0#0)^2
    )else f = f + r_(1,0,l)(g_0)*(g_1)*(g_2) +  r_(1,0,l)(g_1)*(g_0)*(g_2) +  r_(1,0,l)(g_2)*(g_1)*(g_0);
    return f;
    )






GAquart = (l,t) -> (
    f := 0;
    g := toList factor t/value;
    if length(g) == 1 then (
        op := factor t; 
        f = f + 4*r_(1,0,l)(op#0#0)*(op#0#0)^3;
    )else if length(g) == 2 then (
        g0 := factor g_0;
        g1 := factor g_1;
        if g0#0#1 == 2 then (
          f = f + 2*r_(1,0,l)(g0#0#0)*(g0#0#0)*(g1#0#0)^2 + 2*r_(1,0,l)(g1#0#0)*(g1#0#0)*(g0#0#0)^2;
        )else if g0#0#1 == 3 then (
            f = f + 3*r_(1,0,l)(g0#0#0)*(g1#0#0)*(g0#0#0)^2 + r_(1,0,l)(g1#0#0)*(g0#0#0)^3;
        )else if g0#0#1 == 1 then (
        f = f + 3*r_(1,0,l)(g1#0#0)*(g0#0#0)*(g1#0#0)^2 + r_(1,0,l)(g0#0#0)*(g1#0#0)^3;
        );
    )else if length(g) == 3 then (
        g0 = factor g_0;
        g1 = factor g_1;
        g2 := factor g_2;
        if g0#0#1 == 2 then (
            f = f + 2*r_(1,0,l)(g0#0#0)*(g0#0#0)*(g1#0#0)*(g2#0#0) + r_(1,0,l)(g1#0#0)*(g0#0#0)*(g0#0#0)*(g2#0#0) +  r_(1,0,l)(g2#0#0)*(g0#0#0)*(g0#0#0)*(g1#0#0); 
        )else if g0#0#1 == 1 then (
            if g1#0#1 == 1 then (
                f = f + 2*r_(1,0,l)(g2#0#0)*(g2#0#0)*(g1#0#0)*(g0#0#0) + r_(1,0,l)(g1#0#0)*(g0#0#0)*(g2#0#0)*(g2#0#0) +  r_(1,0,l)(g0#0#0)*(g2#0#0)*(g2#0#0)*(g1#0#0);
            )else  f = f + 2*r_(1,0,l)(g1#0#0)*(g0#0#0)*(g1#0#0)*(g2#0#0) + r_(1,0,l)(g0#0#0)*(g1#0#0)*(g1#0#0)*(g2#0#0) +  r_(1,0,l)(g2#0#0)*(g1#0#0)*(g0#0#0)*(g1#0#0); 
        );
    )else f = f + r_(1,0,l)(g_0)*(g_1)*(g_2)*(g_3) + r_(1,0,l)(g_1)*(g_0)*(g_2)*(g_3) + r_(1,0,l)(g_2)*(g_1)*(g_0)*(g_3) + r_(1,0,l)(g_3)*(g_1)*(g_2)*(g_0);
    return f;
    )

G = (d,l,t) -> (
    GG := {};
    if t != 0 then (
        (M,C) := coefficients t;
        M = flatten entries M;
        C = flatten entries C;
        for i from 0 to length(M)-1 do 
            if d==3 then(
                GG = append(GG,(C_i)*GAcub(l,M_i));
            )else(
                GG=append(GG,(C_i)*GAquart(l,M_i));
            );
        GG = sum(GG);
    )else GG = 0;
    return GG;
    )

deg3factors=(I,hwvq,lm,d,Y,ML)->(
    Jac := jacobian I;--diff(transpose basis(1,S),matrix mingens I);
    KK := mingens kernel transpose Jac;
    col := numColumns KK;
    for i from 0 to col-1 do (
        Q_i = {};
        for j from 0 to length(Y)-1 do (
            Q_i = append(Q_i, a_j => (KK_i)_j);
        );
        hwv_i = sub(hwvq,Q_i);
        J_i = ideal(hwv_i);
    );
    for s from 0 to col-1 do (
        for i from 0 to length(lm)-1 do (
            J_s = J_s + ideal(G(d,lm_i,hwv_s));
        );
        for i from 0 to length(lm)-1 do (
            for j from 0 to length(lm)-1 do (
                J_s = J_s + ideal(G(d,lm_i,G(d,lm_j,hwv_s)));
            );   
        );
        for i from 0 to length(lm)-1 do (
            for j from 0 to length(lm)-1 do (
                for k from 0 to length(lm)-1 do (
                    J_s = J_s + ideal(G(d,lm_i,G(d,lm_j,G(d,lm_k,hwv_s))));
                );
            );
        );
        for i from 0 to length(lm)-1 do (
            for j from 0 to length(lm)-1 do (
                for k from 0 to length(lm)-1 do (
                    for l from 0 to length(lm)-1 do (
                        J_s = J_s + ideal(G(d,lm_i,G(d,lm_j,G(d,lm_k,G(d,lm_l,hwv_s)))));
                    );
                );
            );--J1, J2 and J3  will be the three addends S_(2,1) four times. Now for both of
        );  --them we must compute Sym^3       
    );
    for s from 0 to col-1 do (  
        Tw_s = flatten entries mingens J_s;
        for i from 0 to length(Tw_s)-1 do (
            for j from 0 to length(ML)-1 do (
                J_s = J_s + ideal(G(d,ML_j,(Tw_s)_i),G(d,ML_j,G(d,ML_j,(Tw_s)_i)),G(d,ML_j,G(d,ML_j,G(d,ML_j,(Tw_s)_i))));
            );
        );--this loop generates the factors Sym^3 for all the copies of S_(2.1) four times.

    ); 
    gg := n -> sum for i from 0 to n list J_i;
    JJ := gg (col-1);
    return flatten entries mingens JJ;
)

degree3eqn=(R,W,d,n)->(
    LM := subsets(toList(0..n-1),4);
    cub := {};
    for lm in LM do(
        (Ri,hwvq,Y,ML) := fromListToHwvq(d,lm,n,R);
        for l from 0 to n-1 do(--usate anche per cub e quart
            y_(0,1,l) = map(Ri,Ri,e(0,1,l,Ri));---l fattore cub quart
            r_(1,0,l) = map(Ri,Ri,e(1,0,l,Ri));
        );
        rel := listOfrel(d,hwvq,n);  
        I := ideal(rel);
        S := coefficientRing Ri;
        I = sub(ideal(mingens I),S);
        cub = append(cub,deg3factors(I,hwvq,lm,d,Y,ML));
    );
    return cub;
)

deg4factors=(I,hwvq,d,Y,ML)->(
    Jac := jacobian I; -- diff(transpose basis(1,S),matrix mingens I);
    KK := mingens kernel transpose Jac;
    col := numColumns KK;
    for i from 0 to col-1 do (
        Q_i = {};
        for j from 0 to length Y-1 do (
            Q_i = append(Q_i, a_j => (KK_i)_j);
        );
        hwv_i = sub(hwvq,Q_i);
        J_i = ideal(hwv_i);
    );
    for s from 0 to col-1 do (  
        Tw_s = flatten entries mingens J_s;
        for i from 0 to length(Tw_s)-1 do (
            for j from 0 to length(ML)-1 do (
                J_s = J_s + ideal(G(d,ML_j,(Tw_s)_i),G(d,ML_j,G(d,ML_j,(Tw_s)_i)),G(d,ML_j,G(d,ML_j,G(d,ML_j,(Tw_s)_i))),G(d,ML_j,G(d,ML_j,G(d,ML_j,G(d,ML_j,(Tw_s)_i)))));
            );
        );
    ); 
    gg := n -> sum for i from 0 to n list J_i;
    JJ := gg (col-1);
    return flatten entries mingens JJ;
)

degree4eqn=(R,W,d,n)->(
    LM := subsets(toList(0..n-1),3);
    quart := {};
    for lm in LM do(
        (Ri,hwvq,Y,ML) := fromListToHwvq(d,lm,n,R);
        for l from 0 to n-1 do(--usate anche per cub e quart
            y_(0,1,l) = map(Ri,Ri,e(0,1,l,Ri));---l fattore cub quart
            r_(1,0,l) = map(Ri,Ri,e(1,0,l,Ri));
        );
        rel := listOfrel(d,hwvq,n); 
        I := ideal(rel);
        S := coefficientRing Ri;
        I = sub(ideal(mingens I),S);
        quart = append(quart,deg4factors(I,hwvq,d,Y,ML));
    );
    return quart;
)


----------------------------------------
--ex. equations tangential variety of Segre of (PP^1)^5
--n=5
idealTangential=(n)->(
    (R,W) := infoTangential(n);
    deg2eqn := degree2eqn(R,W,2,n); --grado 2
    deg2eqn = flatten(deg2eqn);
    deg2eqn = apply(deg2eqn,x->sub(x,R));  
    deg3eqn := degree3eqn(R,W,3,n); --grado 3
    deg3eqn = flatten(deg3eqn);
    deg3eqn = apply(deg3eqn,x->sub(x,R));
    deg4eqn := degree4eqn(R,W,4,n); --grado 4 ---tempo per n=5 9 minuti
    deg4eqn = flatten(deg4eqn);
    deg4eqn = apply(deg4eqn,x->sub(x,R));
    gener := {deg2eqn,deg3eqn,deg4eqn};
    gener = flatten gener;
    return ideal(gener);
)








