####################
 Julia
####################


- :doc:`Adjoints/index`

- :doc:`BayesianIntegrals/index`

- :doc:`CoxHomotopies/index`

- :doc:`CountingChambers/index`

- :doc:`FiberZonotopes/index`

- :doc:`GaussianEntropyMap/index`

- :doc:`GenerationOfMatroids/index`

- :doc:`GeometriesJordanNetsWebs/index`

- :doc:`27pAdicLines/index`

- :doc:`ToricGeometry/index`

- :doc:`Landau/index`

- :doc:`LikelihoodDegenerations/index`

- :doc:`MarginalIndependence/index`

- :doc:`OSCAR/index`

- :doc:`OrdersPolytropes/index`

- :doc:`QuinticSpectrahedra/index`

- :doc:`BranchPoints/index`

- :doc:`circlesTangentConics/index`

- :doc:`twopareig_alternating/index`

- :doc:`TangentQuadricsInThreeSpace/index`

- :doc:`EulerIntegrals/index`




.. toctree::
   :maxdepth: 0
