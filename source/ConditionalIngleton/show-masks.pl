#!/usr/bin/env perl

use utf8::all;
use Modern::Perl;
use CInet::Base;
use List::MoreUtils qw(zip6);

my @sq = Cube(4)->squares;
while (<>) {
    my @c = m/-?\d+/g;
    my $a = pop @c;
    say sprintf("%+d□(12)", -$a), " = ", join(" ",
        map { sprintf "%+d△(%s|%s)", $_->[1], join("", $_->[0][0]->@*), join("", $_->[0][1]->@*) }
        grep { $_->[1] }
        zip6 @sq, @c
     );
}
