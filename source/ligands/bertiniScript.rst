.. ligands_idealScript:

#############
Using Bertini
#############

Bertini is a free software for solving polynomial systems numerically using homotoy continuation. It can be downloaded here: https://bertini.nd.edu/

Computing roots numerically
===========================

To run Bertini on a valid input file, run

.. code-block:: none

   bash-3.2$ bertini binding43.bertini

Bertini will then create files in the current directory containing the solutions. For large systems, it is recommended to run Bertini in parallel:

.. code-block:: none

   bash-3.2$ mpirun -np 16 bertini binding43.bertini

More information on how to run bertini can be found on its website.
