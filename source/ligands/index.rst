=======================================
Binding Polynomials of Target Molecules
=======================================

.. image:: banner.png

In biology, a ligand is a substance that binds to a target molecule to serve a given purpose. A classical example is oxygen, which binds reversibly to hemoglobin to be transported through the bloodstream. Another example would be protons, which bind to receptors in neurons for signal transduction.

In this context, the binding polynomial, arises as the denominator of the rational function describing the average number of occupied binding sites as a function of ligand activity. Its roots play an important role for the characterization of the binding behavior of the ligand to the target molecule.

| Here, we  present some scripts for the experimantal results presented in the following paper, which studied the case in which there are two types of ligands binding to the same target molecule:
| Yue Ren, Johannes W. R. Martini, and Jacinta Torres: Decoupled molecules with binding polynomials of bidegree (n,2)
| In: Journal of mathematical biology, 78 (2019) 4, p. 879-898
| MIS-Preprint: `75/2017 <https://www.mis.mpg.de/publications/preprints/2017/prepr2017-75.html>`_ DOI: `10.1007/s00285-018-1295-x <https://dx.doi.org/10.1007/s00285-018-1295-x>`_ ARXIV: https://arxiv.org/abs/1711.06865 CODE: https://mathrepo.mis.mpg.de/ligands

.. toctree::
   :maxdepth: 2

   singularScript.rst
   bertiniScript.rst
