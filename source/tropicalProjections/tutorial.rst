########
Tutorial
########

Here is a minimal and not-so-minimal example of computing a zero-dimensional tropical variety using the functions in the library :download:`tropicalProjection.lib <tropicalProjection.lib>`.

.. code-block:: none

                       SINGULAR                                /  Development
  A Computer Algebra System for Polynomial Computations       /   version 4.1.1
                                                           0<
  by: W. Decker, G.-M. Greuel, G. Pfister, H. Schoenemann     \   Feb 2018
  FB Mathematik der Universitaet, D-67653 Kaiserslautern       \
  > LIB "tropicalProjection.lib";
  > ring r = 0,(w,z,y,x),lp;
  > number uniformizingParameter = 2;
  > ideal I =
  .   (x-1)*(x-2)*(x-4)*(x-6)*(x-8),
  .   y-2x,
  .   z-2y,
  .   w-2z;
  > ideal stdI = std(I);
  > list TropI1 = tropicalVarietyViaProjection(stdI,uniformizingParameter);
  > list mergeStrat = getStrat("overlap");
  > string elimStrat = "minpoly";
  > int useModular = 1;
  > int useParallel = 1
  > list TropI2 = tropicalVarietyViaProjection(stdI,uniformizingParameter,mergeStrat,elimStrat,useModular,useParallel);

Please note that the library requires a lexicographical Groebner basis of an ideal in shape position. Functions for coaxing degenerate ideals into shape position will soon follow.

The optional parameters are

* mergeStrat: the strategy of gluing the projections (default: getStrat("overlap"))
* elimStrat: the strategy of computing eliminations (default: minpoly)
* useModular: 1 if modular approach should be used, 0 otherwise (default: 1 if char(basering)=0, 0 otherwise)
* useParallel: 1 if parallelization should be used; 0 otherwise (default: 0)

For a list of possible strategies, please see the documentation.
