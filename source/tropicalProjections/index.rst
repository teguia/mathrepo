=============================================================
Computing Zero-Dimensional Tropical Varieties via Projections
=============================================================

.. image:: tropicalProjection.png



| This page contains auxilliary material for the following preprint:
| Paul Görlach, Yue Ren, and Leon Zhang: Computing zero-dimensional tropical varieties via projections
| In: Computational complexity, 31 (2022) 1 , 5
| MIS-Preprint: `79/2019 <https://www.mis.mpg.de/publications/preprints/2019/prepr2019-79.html>`_ ARXIV: https://arxiv.org/abs/1908.03486 CODE: https://mathrepo.mis.mpg.de/tropicalProjections

All algorithms are implemented in the Singular_ library :download:`tropicalProjection.lib <tropicalProjection.lib>`, the magma_ library for approximating the roots in the non-Archimedean which was used for the timings can be found :download:`here <lines_func.m>`.

In addition to a short tutorial on how to use the library, this website contains a database with all examples that were used for the timings.

.. _Singular: https://www.singular.uni-kl.de/

.. _magma: http://magma.maths.usyd.edu.au/magma/

.. toctree::
   :maxdepth: 2

   tutorial.rst
   examples.rst
