lookupTable = {{X,x},{y,Y}};
word[px_,qx_]:=(
    p=px/GCD[px,qx]; q=qx/GCD[px,qx];
    length = 2 q;
    wd = {};
    Do[(
        height = i*p/q;
        height = If[Ceiling[height] == height,height+1/2,height];
        AppendTo[wd,lookupTable[[If[Mod[i,2]==1,1,2],If[Mod[Ceiling[height],2]==1,1,2]]]];
        ), {i,length}];
    wd);

XX={{a,1},{0,a^(-1)}};
xx=Inverse[XX];
YY={{b,0},{\[Rho],b^(-1)}};
yy=Inverse[YY];

farey[slopes_]:= (
    wordList=Map[{#,word @@ #}&,slopes];
    matrixProducts = Map[{#[[1]],Apply[Dot,#[[2]]]}&,wordList];
    matrixProductsSubstituted=matrixProducts/.{X->XX,Y->YY,x->xx,y->yy};
    Map[{#[[1]],Collect[Expand[Simplify[Tr[#[[2]]]]],\[Rho]]}&,matrixProductsSubstituted]
    );
coprime[q_]:=Select[Range[q],GCD[#,q]==1&];
qs=Range[10];
fareysForDenom[q_]:=(
    ps = coprime[q];
    Map[farey[{{#,q}}]&,ps]
    );
    
fareyList=Fold[Function[{head,q},Join[head,Flatten[fareysForDenom[q]/.\[Rho]->z,1]]],{},qs];
fareyList>>"fareys_general.txt"
